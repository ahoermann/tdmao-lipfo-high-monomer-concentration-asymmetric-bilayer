static double
f_constant(double q, double r, double sld)
{
  const double bes = sas_3j1x_x(q * r);
  const double vol = M_4PI_3 * cube(r);
  return sld * vol * bes;
}

static double
radius(double volume, double bilayer_thickness)
{
  // r: radius at the bilayer center 
  double r = sqrt((volume - M_PI/3.*cube(bilayer_thickness))/(4*M_PI*bilayer_thickness));
  return r;
}


static double
outer_radius(double volume, double bilayer_thickness)
{
  double r = radius(volume, bilayer_thickness);
  return r + bilayer_thickness/2.;
}

static double
shell_volume(double volume, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3,  double v_surf1, double v_surf2, double v_surf1_tail, double v_surf2_tail)
{
  return volume;
}

static double
form_volume(double volume, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3, double v_surf1, double v_surf2, double v_surf1_tail, double v_surf2_tail)
{
  return M_4PI_3 * cube(outer_radius(volume, bilayer_thickness));
}


static double
radius_effective(int mode, double volume, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3, double v_surf1, double v_surf2, double v_surf1_tail, double v_surf2_tail)
{
  switch (mode) {
  default:
  case 1: // outer radius
    return outer_radius(volume, bilayer_thickness);
  case 2: // core radius
    double r = radius(volume, bilayer_thickness);
    return r - bilayer_thickness;
  }
}

double cardano_real_root(double a, double b, double c, double d)
{
    double Q(double a, double b, double c)
      {
        return (3*a*c-square(b))/(9*square(a));
      };
    double r(double a,double b,double c,double d)
      {
        return (9*a*b*c - 27*square(a)*d - 2*cube(b))/(54*cube(a));
      };
    double s = cbrt(r(a,b,c,d) + sqrt(cube(Q(a,b,c)) + square(r(a,b,c,d))));
    double t = cbrt(r(a,b,c,d) - sqrt(cube(Q(a,b,c)) + square(r(a,b,c,d))));
    double real_root = s + t - b/(3*a);
    return real_root;
}

void  
derived_profile(double *slds, double *t_hg, double core_radius, double thickness_bilayer,  double sld_bilayer, double x_surf1, double thickness_tails, double x_methyl_c, double x_surf2_o, double sld_surf1, double sld_surf2, double v_surf1, double v_surf2, double v_surf1_tail, double v_surf2_tail, double sl_surf1_tail, double sl_surf2_tail, double sl_methyl, double sl_methylene)
{
  double ro = core_radius + thickness_bilayer;
  double v_bilayer = M_4PI_3 * (cube(ro) - cube(core_radius));
  double sl_bilayer = sld_bilayer * v_bilayer;
  double v_avg = x_surf1 * v_surf1 + (1-x_surf1) * v_surf2;
  double n_surf2 = v_bilayer/v_avg * (1-x_surf1);
  double n_surf1 = v_bilayer/v_avg * (x_surf1);
  double surface_out = 4*M_PI*ro*ro;
  double surface_in = 4*M_PI*core_radius*core_radius;
  double a_avg = (surface_out + surface_in)/(n_surf1 + n_surf2);
  double n_out = surface_out/a_avg;
  double n_in = surface_in/a_avg;
  double v_hg_o = n_out * ((1-x_surf2_o) * (v_surf1 - v_surf1_tail) + x_surf2_o * (v_surf2 - v_surf2_tail));
  double x_surf2_i = (n_surf2 - x_surf2_o*n_out)/n_in;
  double v_hg_i = n_in * ((1-x_surf2_i) * (v_surf1 - v_surf1_tail) + x_surf2_i * (v_surf2 - v_surf2_tail));
  double thickness_hg_i = cardano_real_root(1, 3*core_radius, 3*square(core_radius), -v_hg_i/M_4PI_3);
  double thickness_hg_o = cardano_real_root(-1, 3*ro, -3*square(ro), v_hg_o/M_4PI_3);
  double sl_surf1 = sld_surf1 * n_surf1 * v_surf1;
  double v_tot_surf2 = n_surf2 * v_surf2;
  double v_tot_surf1 = n_surf1 * v_surf1;
  double ch3_v = 52.7e-3;
  double ch2_v = 28.1e-3;
  double sld_methyl = sl_methyl/ch3_v;
  double sld_methylene = sl_methylene/ch2_v;
  double sld_c = (x_methyl_c * ch3_v * sld_methyl + (1-x_methyl_c) * ch2_v * sld_methylene)/(x_methyl_c * ch3_v + (1-x_methyl_c) * ch2_v);
  double r1 = core_radius + thickness_hg_i;
  double r2 = core_radius + thickness_hg_i + thickness_tails;
  double  r3 = ro - thickness_hg_o - thickness_tails;
  double  r4 = ro - thickness_hg_o;
  double  v_c = (cube(r3) - cube(r2))* M_4PI_3;
  double  v_t_i = (cube(r2) - cube(r1))* M_4PI_3;
  double  v_t_o = (cube(r4) - cube(r3))* M_4PI_3;
  double  sl_surf1_outer = sl_surf1_tail * n_surf1 - v_c * sld_c;
  double  v_tot_surf1_outer = n_surf1 * v_surf1_tail - v_c;
  double  v_surf1_outer = v_tot_surf1_outer/n_surf1;
  double  phi_surf1_tail = (v_t_i + v_t_o - v_surf2_tail*n_surf2)/(v_t_i + v_t_o);
  double  sld_surf1_tail = sl_surf1_outer/((v_t_i + v_t_o) * phi_surf1_tail); // from remaining scattering length, same inside and outside
  double  v_surf2_t_o = x_surf2_o * n_out * v_surf2_tail;
  double  phi_surf2_t_o = v_surf2_t_o/v_t_o;
  double  sld_t_o = ((1-phi_surf2_t_o) * sld_surf1_tail + phi_surf2_t_o * sl_surf2_tail/v_surf2_tail);
  double  sl_surf2_hg = sld_surf2 * v_surf2 - sl_surf2_tail;
  double  sl_surf1_hg = sld_surf1 * v_surf1 - sl_surf1_tail;
  double  v_surf2_hg = v_surf2 - v_surf2_tail;
  double  v_surf1_hg = v_surf1 - v_surf1_tail;
  double  phi_surf2_hg_o = n_out * (x_surf2_o * v_surf2_hg)/v_hg_o;
  double sld_hg_o = phi_surf2_hg_o * (sl_surf2_hg/v_surf2_hg) + (1-phi_surf2_hg_o) * (sl_surf1_hg/v_surf1_hg);
  double  phi_surf2_hg_i = n_in * (x_surf2_i * v_surf2_hg)/v_hg_i;
  double  sld_hg_i = phi_surf2_hg_i * (sl_surf2_hg/v_surf2_hg) + (1-phi_surf2_hg_i) * (sl_surf1_hg/v_surf1_hg);
  double  sld_t_i = (sl_bilayer - v_c * sld_c - v_t_o * sld_t_o - v_hg_i * sld_hg_i - v_hg_o * sld_hg_o)/v_t_i;
  slds[0] = sld_hg_i;
  slds[1] = sld_t_i;
  slds[2] = sld_c;
  slds[3] = sld_t_o;
  slds[4] = sld_hg_o;
  t_hg[0] = thickness_hg_i;
  t_hg[1] = thickness_hg_o;
}




static void
Fq(double q, double *F1, double *F2, double volume, double solvent_sld, double bilayer_sld, double bilayer_thickness, double x_surf1, double thickness_tails, double x_methyl_c, double x_surf2_o, double sld_surf1, double sld_surf2, double v_surf1, double v_surf2, double v_surf1_tail, double v_surf2_tail, double sl_surf1_tail, double sl_surf2_tail, double sl_methyl, double sl_methylene)
{
  double f, r, last_sld;
  double r_center = radius(volume, bilayer_thickness);
  double core_radius = r_center - bilayer_thickness/2.;
  double slds[5] = {};
  double t_hg[2] = {};
  derived_profile(slds, t_hg, core_radius, bilayer_thickness, bilayer_sld, x_surf1, thickness_tails, x_methyl_c, x_surf2_o, sld_surf1, sld_surf2, v_surf1, v_surf2, v_surf1_tail, v_surf2_tail, sl_surf1_tail, sl_surf2_tail, sl_methyl, sl_methylene);
  double sld_hg_i = slds[0];
  double sld_t_i = slds[1];
  double sld_c = slds[2];
  double sld_t_o = slds[3];
  double sld_hg_o = slds[4];
  double t_hg_i = t_hg[0];
  double t_hg_o = t_hg[1];
  double t_2 = bilayer_thickness - 2*thickness_tails - t_hg_i - t_hg_o;
  r = core_radius;
  last_sld = solvent_sld;
  f = 0.;
  // shell 1
  f += M_4PI_3 * cube(r) * (sld_hg_i - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld_hg_i;
  r += t_hg_i;
  // shell 2
  f += M_4PI_3 * cube(r) * (sld_t_i - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld_t_i;
  r += thickness_tails;
  // shell 3
  f += M_4PI_3 * cube(r) * (sld_c - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld_c;
  r += t_2;
  // shell 4
  f += M_4PI_3 * cube(r) * (sld_t_o - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld_t_o;
  r += thickness_tails;
  // shell 4
  f += M_4PI_3 * cube(r) * (sld_hg_o - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld_hg_o;
  r += t_hg_o;
  f += M_4PI_3 * cube(r) * (solvent_sld - last_sld) * sas_3j1x_x(q*r);
  *F1 = 1e-2 * f;
  *F2 = 1e-4 * f * f * q * q;
}
