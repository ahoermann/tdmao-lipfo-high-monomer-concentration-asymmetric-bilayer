from __future__ import division

import numpy as np
from numpy import inf
from numpy import sqrt 

name = "vesicles_3sh_symm_volume"
title = "This model provides the scattering from a spherical shell with 3 \
 concentric shell structures. The SLD of the innermost and outermost shell are \
 calculated from the average sld of the shell and the sld of the shell at the bilayer center.\
 The thickness of the central shell is calculated from the total thickness and the thickness\
 of the inner and outer shell. The radius is calculated from the aggregate volume and the total\
 thickness."

description = """\ Form factor for a 3-shell vesicle normalized by the volume.  The
sld profile of the bilayer is constrained to be symmetrical. 

    scale: equal to the volume fraction if data is on absolute scale
    background: incoherent background 
    volume: the volume of the aggregate (bilayer)
    sld_solvent: the SLD of the solvent (inside and outside)
    sld_bilayer: the average SLD of the bilayer
    thickness_bilayer: the overall thickness of the bilayer
    sld2: the SLD of the central shell of the bilayer 
    thickness13: the thickness of the innermost and outermost shells


"""

category = "shape:sphere"


#             ["name", "units", default, [lower, upper], "type","description"],
parameters = [["volume", "nm^3", 4000., [0, inf], "volume", "Volume of the aggregate (bilayer)"],
              ["sld_solvent", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Solvent scattering length density"],
              ["sld_bilayer", "1e-6/Ang^2", 1.7, [-inf, inf], "sld", "Average bilayer scattering length density"],
              ["thickness_bilayer", "Ang", 2.6, [0, inf], "volume", "Overall thickness of the bilayer"],
              ["x_surf1", "", 0.5, [0, 1], "volume", "molar fraction of surfactant 1"],
              ["thickness13", "Ang", 0.984, [0, inf], "volume", "Thickness of shells 1/3"],
              ["x_methyl_2", "", 0.1667, [0, 1], "volume", "Fraction of methyl groups in shell 2"],
              ["sld_surf1", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Surfactant 1 scattering length density"],
              ["sld_surf2", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Surfactant 2 scattering length density"],
              ["v_surf1", "nm^3", 0.482, [-inf, inf], "volume", "Surfactant 1 molecular volume"],
              ["v_surf2", "nm^3", 0.356, [-inf, inf], "volume", "Surfactant 2 molecular volume"],
              ["sl_methyl", "1e-6 Ang", 0.482, [-inf, inf], "sld", "Scattering length of a methyl group"],
              ["sl_methylene", "1e-6 Ang", 0.356, [-inf, inf], "sld", "Scattering length of a methylene group"],
             ]

source = ["lib/sas_3j1x_x.c", "vesicles_3sh_symm_volume_kratky.c"]
have_Fq = True
radius_effective_modes = ["outer radius", "core radius"]

def profile(volume, sld_solvent, sld_bilayer,  thickness_bilayer, x_surf1, thickness13, x_methyl_2, sld_surf1, sld_surf2, v_surf1, v_surf2, sl_methyl, sl_methylene):
    """
    Returns the SLD profile *r* (Ang), and *rho* (1e-6/Ang^2).
    """

    def radius_from_v(volume, t):
        radius = sqrt((volume - c.pi/3.*t**3)/(4*c.pi*t))
        return radius

    radius = radius_from_v(volume, thickness_bilayer)

    ri = radius - thickness_bilayer/2. 
    t2 = thickness_bilayer - 2 * thickness13
    r1 = ri + thickness13
    r2 = ri + thickness_bilayer - thickness13
    v_bilayer = ((ri + thickness_bilayer)**3 - ri**3)
    sl_bilayer = sld_bilayer * v_bilayer
    v_avg = x_surf1 * v_surf1 + (1-x_surf1) * v_surf2
    n_surf2 = v_bilayer/v_avg * (1-x_surf1)
    n_surf1 = v_bilayer/v_avg * (x_surf1)
    sl_surf1 = sld_surf1 * n_surf1 * v_surf1
    v_tot_surf2 = n_surf2 * v_surf2
    v_tot_surf1 = n_surf1 * v_surf1
    ch3_v=52.7e-3
    ch2_v = 28.1e-3
    sld_methyl = sl_methyl/ch3_v
    sld_methylene = sl_methylene/ch2_v
    sld2 = (x_methyl_2 * ch3_v * sld_methyl + (1-x_methyl_2) * ch2_v * sld_methylene)/(x_methyl_2 * ch3_v + (1-x_methyl_2) * ch2_v)
    v_2 = r2**3 - r1**3
    sl_surf1_outer = sld_surf1 * v_tot_surf1 - v_2 * sld2 
    v_13 = r1**3 - ri**3 + (ri+thickness_bilayer)**3 - r2**3
    phi_surf1_13 = (v_13 - v_tot_surf2)/v_13
    sld_surf1_outer = sl_surf1_outer/(v_13 * phi_surf1_13)
    sld_outer = phi_surf1_13 * sld_surf1_outer + (1-phi_surf1_13) * sld_surf2
    sl_2 = sld2 * (r2**3 - r1**3)
    sld13 = (sl_bilayer - sl_2)/(r1**3 - ri**3 + (ri+thickness_bilayer)**3 - r2**3)
    z = []
    rho = []
    radii = [0.95*ri, ri, r1, r2, ri + thickness_bilayer, (ri + thickness_bilayer)*1.05]
    slds = [sld_solvent, sld13, sld2, sld13, sld_solvent]
    k = 0
    for sld in slds:
        z.append(radii[k])
        rho.append(sld)
        z.append(radii[k+1])
        rho.append(sld)
        k += 1 
        
    return np.asarray(z), np.asarray(rho)
