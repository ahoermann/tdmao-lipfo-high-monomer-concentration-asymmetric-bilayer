from numpy import inf

from sasmodels.core import reparameterize

parameters = [
    # name, units, default, [min, max], type, description
    ["c_molar", "mol/L", 9e-3, [0, inf], "", "surfactant concentration"],
    ["x_surf1_sample", "nm^3", 0.345, [0, inf], "", "surfactant molecular volume"],
    ["x_surf1_vesicle", "nm^2", 0.0005654044852836229, [0, inf], "", "surfactant scattering length"],
    ["sld_surf1", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Surfactant 1 scattering length density"],
    ["sld_surf2", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Surfactant 2 scattering length density"],
    ["v_surf1", "nm^3", 0.482, [-inf, inf], "volume", "Surfactant 1 molecular volume"],
    ["v_surf2", "nm^3", 0.356, [-inf, inf], "volume", "Surfactant 2 molecular volume"],
    ]

translation = """
x_surf2_monomer = 1/(1-x_surf1_sample) - x_surf1_sample/(x_surf1_vesicle * (1-x_surf1_sample))
c_surf2_monomer = c_molar*(1-x_surf1_sample)*x_surf2_monomer
volfraction = 6.02214076e+23*(c_molar - c_surf2_monomer)*1e-24*(v_surf1*x_surf1_vesicle + v_surf2*(1-x_surf1_vesicle))
sld = (sld_surf1 * v_surf1 * x_surf1_vesicle + sld_surf2 * v_surf2 * (1-x_surf1_vesicle))/(v_surf1 * x_surf1_vesicle + v_surf2 * (1-x_surf1_vesicle))
"""

model_info = reparameterize('custom.vesicle', parameters, translation, __file__)
