from __future__ import division

import numpy as np
from numpy import inf, sqrt, cbrt

name = "vesicle_5sh_volume"
title = "This model provides the scattering from a spherical shell with 3 \
 concentric shell structures. The SLD of the innermost and outermost shell are \
 calculated from the average sld of the shell and the sld of the shell at the bilayer center.\
 The thickness of the central shell is calculated from the total thickness and the thickness\
 of the inner and outer shell."

description = """\ Form factor for a 3-shell vesicle normalized by the volume.  The
sld profile of the bilayer is constrained to be symmetrical. 

    scale: equal to the volume fraction if data is on absolute scale
    background: incoherent background 
    volume: the volume of the shell 
    sld_solvent: the SLD of the solvent (inside and outside)
    sld_bilayer: the average SLD of the bilayer
    thickness_bilayer: the overall thickness of the bilayer
    sld2: the SLD of the central shell of the bilayer 
    thickness13: the thickness of the innermost and outermost shells


"""

category = "shape:sphere"


#             ["name", "units", default, [lower, upper], "type","description"],
parameters = [["volume", "nm^3", 4000., [0, inf], "volume", "Volume of the aggregate (bilayer)"],
              ["sld_solvent", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Solvent scattering length density"],
              ["sld_bilayer", "1e-6/Ang^2", 1.7, [-inf, inf], "sld", "Average bilayer scattering length density"],
              ["thickness_bilayer", "Ang", 2.6, [0, inf], "volume", "Overall thickness of the bilayer"],
              ["x_surf1", "", 0.5, [0, 1], "volume", "molar fraction of surfactant 1"],
              ["thickness_mixedtails", "Ang", 0.984, [0, inf], "volume", "Thickness of mixed tail shells"],
              #["thickness_hg", "Ang", 0.3, [0, inf], "volume", "Thickness of the headgroup shells"],
              ["x_methyl_c", "", 0.1667, [0, 1], "volume", "Fraction of methyl groups in the central shell"],
              ["x_surf2_o", "", 0.65, [0, 1], "volume", "molar fraction of surfactant 2 in the outermost shell"],
              ["sld_surf1", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Surfactant 1 scattering length density"],
              ["sld_surf2", "1e-6/Ang^2", 6.4, [-inf, inf], "sld", "Surfactant 2 scattering length density"],
              ["v_surf1", "nm^3", 0.482, [-inf, inf], "volume", "Surfactant 1 molecular volume"],
              ["v_surf2", "nm^3", 0.356, [-inf, inf], "volume", "Surfactant 2 molecular volume"],
              ["v_surf1_tail", "nm^3", 0.482, [-inf, inf], "volume", "Surfactant 1 molecular volume (tail)"],
              ["v_surf2_tail", "nm^3", 0.356, [-inf, inf], "volume", "Surfactant 2 molecular volume (tail)"],
              ["sl_surf1_tail", "1e-6 Ang", 0.482, [-inf, inf], "sld", "Surfactant 1 scattering length (tail)"],
              ["sl_surf2_tail", "1e-6 Ang", 0.356, [-inf, inf], "sld", "Surfactant 2 scattering length (tail)"],
              ["sl_methyl", "1e-6 Ang", 0.482, [-inf, inf], "sld", "Scattering length of a methyl group"],
              ["sl_methylene", "1e-6 Ang", 0.356, [-inf, inf], "sld", "Scattering length of a methylene group"],
             ]

source = ["lib/sas_3j1x_x.c", "vesicle_5sh_volume_kratky.c"]
have_Fq = True
radius_effective_modes = ["outer radius", "core radius"]

def profile(volume, sld_solvent, sld_bilayer, thickness_bilayer,
            x_surf1, thickness_mixedtails, x_methyl_c, x_surf2_o,
            sld_surf1, sld_surf2, v_surf1, v_surf2, v_surf1_tail, v_surf2_tail,
            sl_surf1_tail, sl_surf2_tail, sl_methyl, sl_methylene):
    """
    Returns the SLD profile *r* (Ang), and *rho* (1e-6/Ang^2).
    """

    def radius_from_v(volume, t):
        radius = sqrt((volume - np.pi/3.*t**3)/(4*np.pi*t))
        return radius

    radius = radius_from_v(volume, thickness_bilayer)

    ri = radius - thickness_bilayer/2. 
    four_pi_third = 4*np.pi/3. 
    ro = ri + thickness_bilayer 
    v_bilayer = (ro**3 - ri**3) * four_pi_third
    
    def cubic_real_root(a, b, c, d):
        def q(a,b,c):
            return (3*a*c-b**2)/(9*a**2)
        def r(a,b,c,d):
            return (9*a*b*c - 27*a**2*d - 2*b**3)/(54*a**3)
        s = cbrt(r(a,b,c,d) + sqrt(q(a,b,c)**3 + r(a,b,c,d)**2))
        t = cbrt(r(a,b,c,d) - sqrt(q(a,b,c)**3 + r(a,b,c,d)**2))
        real_root = s + t - b/(3*a)
        return real_root
           
    sl_bilayer = sld_bilayer * v_bilayer
    v_avg = x_surf1 * v_surf1 + (1-x_surf1) * v_surf2
    n_surf2 = v_bilayer/v_avg * (1-x_surf1)
    n_surf1 = v_bilayer/v_avg * (x_surf1)
    surface_out = 4*np.pi*ro**2
    surface_in = 4*np.pi*ri**2
    a_avg = (surface_out + surface_in)/(n_surf1 + n_surf2)
    n_out = surface_out/a_avg
    n_in = surface_in/a_avg
    v_hg_o = n_out * ((1-x_surf2_o) * (v_surf1 - v_surf1_tail) + x_surf2_o * (v_surf2 - v_surf2_tail))
    x_surf2_i = (n_surf2 - x_surf2_o*n_out)/n_in
    v_hg_i = n_in * ((1-x_surf2_i) * (v_surf1 - v_surf1_tail) + x_surf2_i * (v_surf2 - v_surf2_tail))
    thickness_hg_i = cubic_real_root(a=1, b=3*ri, c=3*ri**2, d=-v_hg_i/four_pi_third)
    thickness_hg_o = cubic_real_root(a=-1, b=3*ro, c=-3*ro**2, d=v_hg_o/four_pi_third)
    sl_surf1 = sld_surf1 * n_surf1 * v_surf1
    v_tot_surf2 = n_surf2 * v_surf2
    v_tot_surf1 = n_surf1 * v_surf1
    ch3_v = 52.7e-3
    ch2_v = 28.1e-3
    sld_methyl = sl_methyl/ch3_v
    sld_methylene = sl_methylene/ch2_v
    sld_c = (x_methyl_c * ch3_v * sld_methyl + (1-x_methyl_c) * ch2_v * sld_methylene)/(x_methyl_c * ch3_v + (1-x_methyl_c) * ch2_v)
    r1 = ri + thickness_hg_i
    r2 = ri + thickness_hg_i + thickness_mixedtails
    r3 = ro - thickness_hg_o - thickness_mixedtails
    r4 = ro - thickness_hg_o
    v_c = (r3**3 - r2**3)* four_pi_third
    v_t_i = (r2**3 - r1**3)* four_pi_third
    v_t_o = (r4**3 - r3**3)* four_pi_third
    sl_surf1_outer = sl_surf1_tail * n_surf1 - v_c * sld_c
    v_tot_surf1_outer = n_surf1 * v_surf1_tail - v_c
    v_surf1_outer = v_tot_surf1_outer/n_surf1
    phi_surf1_tail = (v_t_i + v_t_o - v_surf2_tail*n_surf2)/(v_t_i + v_t_o)
    sld_surf1_tail = sl_surf1_outer/((v_t_i + v_t_o) * phi_surf1_tail) # from remaining scattering length, same inside and outside
    v_surf2_t_o = x_surf2_o * n_out * v_surf2_tail 
    phi_surf2_t_o = v_surf2_t_o/v_t_o
    sld_t_o = ((1-phi_surf2_t_o) * sld_surf1_tail + phi_surf2_t_o * sl_surf2_tail/v_surf2_tail)
    sl_surf2_hg = sld_surf2 * v_surf2 - sl_surf2_tail
    sl_surf1_hg = sld_surf1 * v_surf1 - sl_surf1_tail
    v_surf2_hg = v_surf2 - v_surf2_tail
    v_surf1_hg = v_surf1 - v_surf1_tail
    phi_surf2_hg_o = n_out * (x_surf2_o * v_surf2_hg)/v_hg_o
    sld_hg_o = phi_surf2_hg_o * (sl_surf2_hg/v_surf2_hg) + (1-phi_surf2_hg_o) * (sl_surf1_hg/v_surf1_hg)
    phi_surf2_hg_i = n_in * (x_surf2_i * v_surf2_hg)/v_hg_i
    sld_hg_i = phi_surf2_hg_i * (sl_surf2_hg/v_surf2_hg) + (1-phi_surf2_hg_i) * (sl_surf1_hg/v_surf1_hg)
    sld_t_i = (sl_bilayer - v_c * sld_c - v_t_o * sld_t_o - v_hg_i * sld_hg_i - v_hg_o * sld_hg_o)/v_t_i
    z = []
    rho = []
    radii = [0.95*ri, ri, ri + thickness_hg_i, r2, r3, r4, ro, (ro)*1.05]
    slds = [sld_solvent, sld_hg_i, sld_t_i, sld_c, sld_t_o, sld_hg_o, sld_solvent]
    k = 0
    for sld in slds:
        z.append(radii[k])
        rho.append(sld)
        z.append(radii[k+1])
        rho.append(sld)
        k += 1 
        
    return np.asarray(z), np.asarray(rho)        

