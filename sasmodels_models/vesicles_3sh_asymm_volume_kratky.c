static double
f_constant(double q, double r, double sld)
{
  const double bes = sas_3j1x_x(q * r);
  const double vol = M_4PI_3 * cube(r);
  return sld * vol * bes;
}


static double
radius(double volume, double bilayer_thickness)
{
  // r: radius at the bilayer center 
  double r = sqrt((volume - M_PI/3.*cube(bilayer_thickness))/(4*M_PI*bilayer_thickness));
  return r;
}

static double
outer_radius(double volume, double bilayer_thickness)
{
  double r = radius(volume, bilayer_thickness);
  return r + bilayer_thickness/2.;
}

static double
shell_volume(double volume, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3, double v_surf1, double v_surf2)
{
  return volume;
}

static double
form_volume(double volume, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3, double v_surf1, double v_surf2)
{
  return M_4PI_3 * cube(outer_radius(volume, bilayer_thickness));
}


static double
radius_effective(int mode, double volume, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3, double v_surf1, double v_surf2)
{
  switch (mode) {
  default:
  case 1: // outer radius
    return outer_radius(volume, bilayer_thickness);
  case 2: // core radius
    return radius(volume, bilayer_thickness)-bilayer_thickness/2.;
  }
}

void  
sld_13(double *slds, double core_radius, double thickness1, double thickness2, double thickness3, double sld_bilayer, double x_surf1, double x_methyl_2, double x_surf2_3, double sld_surf1, double sld_surf2, double v_surf1, double v_surf2, double sl_methyl, double sl_methylene)
{
  double r1 = core_radius + thickness1;
  double r2 = core_radius + thickness1 + thickness2;
  double r3 = core_radius + thickness1 + thickness2 + thickness3;
  double thickness_bilayer = thickness1 + thickness2 + thickness3; 
  double v_bilayer = M_4PI_3 * (cube(r3) - cube(core_radius));
  double sl_bilayer = sld_bilayer * v_bilayer;
  double v_avg = x_surf1 * v_surf1 + (1-x_surf1) * v_surf2;
  double n_surf2 = v_bilayer/v_avg * (1-x_surf1);
  double n_surf1 = v_bilayer/v_avg * (x_surf1);
  double surface_out = 4*M_PI*r3*r3;
  double surface_in = 4*M_PI*core_radius*core_radius;
  double a_avg = (surface_out + surface_in)/(n_surf1 + n_surf2);
  double n_out = surface_out/a_avg;
  double n_in = surface_in/a_avg;
  double sl_surf1 = sld_surf1 * n_surf1 * v_surf1;
  double v_tot_surf2 = n_surf2 * v_surf2;
  double v_tot_surf1 = n_surf1 * v_surf1;
  double ch3_v = 52.7e-3;
  double ch2_v = 28.1e-3;
  double sld_methyl = sl_methyl/ch3_v;
  double sld_methylene = sl_methylene/ch2_v;
  double sld2 = (x_methyl_2 * ch3_v * sld_methyl + (1-x_methyl_2) * ch2_v * sld_methylene)/(x_methyl_2 * ch3_v + (1-x_methyl_2) * ch2_v);
  double v_1 = M_4PI_3 * (cube(r1) - cube(core_radius));
  double v_2 = M_4PI_3 * (cube(r2) - cube(r1));
  double v_3 = M_4PI_3 * (cube(r3) - cube(r2));
  double sl_surf1_outer = sld_surf1 * v_tot_surf1 - v_2 * sld2;
  double v_13 = v_1 + v_3;
  double phi_surf1_13 = (v_13 - v_tot_surf2)/v_13;
  double sld_surf1_outer = sl_surf1_outer/(v_13 * phi_surf1_13);
  double v_surf2_3 = x_surf2_3 * n_out * v_surf2;
  double phi_surf2_3 = v_surf2_3/v_3;
  double sld_3 = ((1-phi_surf2_3) * sld_surf1_outer + phi_surf2_3 * sld_surf2);
  double sl_3 = sld_3 * v_3;
  double sl_2 = sld2 * v_2;
  double sld_1 = (sl_bilayer - sl_2 - sl_3)/v_1;
  slds[0] = sld_1;
  slds[1] = sld2;
  slds[2] = sld_3;
}




static void
Fq(double q, double *F1, double *F2, double volume, double solvent_sld, double bilayer_sld, double bilayer_thickness, double x_surf1, double thickness13, double x_methyl_2, double x_surf2_3, double sld_surf1, double sld_surf2, double v_surf1, double v_surf2, double sl_methyl, double sl_methylene)
{
  double f, r, last_sld;
  double r_center = radius(volume, bilayer_thickness);
  double core_radius = r_center - bilayer_thickness/2.;
  double t2 = bilayer_thickness - 2*thickness13;
  double slds[3] = {};
  sld_13(slds, core_radius, thickness13, t2, thickness13, bilayer_sld, x_surf1, x_methyl_2, x_surf2_3, sld_surf1, sld_surf2, v_surf1, v_surf2, sl_methyl, sl_methylene);
  double sld3 = slds[2];
  double sld2 = slds[1];
  double sld1 = slds[0];
  r = core_radius;
  last_sld = solvent_sld;
  f = 0.;
  // shell 1
  f += M_4PI_3 * cube(r) * (sld1 - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld1;
  r += thickness13;
  // shell 2
  f += M_4PI_3 * cube(r) * (sld2 - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld2;
  r += t2;
  // shell 3
  f += M_4PI_3 * cube(r) * (sld3 - last_sld) * sas_3j1x_x(q*r);
  last_sld = sld3;
  r += thickness13;
  f += M_4PI_3 * cube(r) * (solvent_sld - last_sld) * sas_3j1x_x(q*r);
  *F1 = 1e-2 * f;
  *F2 = 1e-4 * f * f * q * q;
}
