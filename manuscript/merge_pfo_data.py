xT=0.609684183127342
import numpy as np 

s_lipfo8 = open('/home/anja/Documents/Forschung/LiPFO_TDMAO/LiPFO_pure/20111130_0_0326_rebin_ave_sub386.dat')
w_lipfo8 = open('/home/anja/Documents/Forschung/LiPFO_TDMAO/LiPFO_pure/20111130_2_0326_rebin_ave_sub386.dat')
sl8 = np.loadtxt(s_lipfo8, skiprows=1)
wl8 = np.loadtxt(w_lipfo8, skiprows=1)
s_lipfo8.close()
w_lipfo8.close()
mask_l = np.where((sl8[:,0] > 0.5) & (sl8[:,0] < wl8[5,0]))[0]
data_l8 = np.concatenate((sl8[mask_l,:], wl8[5:,:]))
np.savetxt('./pfo_cylinder/pfo_12p5_merged.txt', 
           data_l8)
