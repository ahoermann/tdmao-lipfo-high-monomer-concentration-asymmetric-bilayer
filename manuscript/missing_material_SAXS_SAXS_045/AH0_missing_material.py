xT=0.609684183127342
store="missing_material"
# following http://www.sasview.org/docs/user/qtgui/Perspectives/Fitting/scripting.html
import numpy as np
from bumps.names import FitProblem 
import sys
from scipy.optimize import fsolve
from scipy.special import logit, expit
import scipy.constants as c 
import pandas as pd 
sys.path.append('../../sasview-5.0.4/src')
sys.path.append('~/sasmodels/custom_models')
sys.path.append('.')
from sasmodels.data import load_data
#from bumps.names import *
from sasmodels.core import load_model
from sasmodels.bumps_model import Model, Experiment
from sasmodels.data import load_data, set_beam_stop, set_top
import os 
import bumps 
from bumps.fitproblem import load_problem
from bumps.cli import load_best
from scipy.stats import gmean

def separate_composite_parameters(par_df):
    """Split a dataframe containing parameters from a composite
model. Input is expected cleaned from leading dots and fit ranges"""
    groups = par_df.name.apply(lambda x: x.split('_')[0]).unique()
    grouped = par_df.groupby(par_df.name.apply(lambda x: x.split('_')[0]))
    separated = {}
    for key, item in grouped:
        if len(key) < 2:
            item.at['{}_scale'.format(key),'value'] *= grouped.get_group('scale').loc['scale'].value
            item.index = item.name.apply(lambda x: x.lstrip('{}_'.format(key)))
            item = item.append(grouped.get_group('background'))
            item.drop(columns=['name'], inplace=True)
            separated[key] = item
    return separated

def errfile_to_df(errfile):
    "does not work for simultaneous fits, load model instead"
    params = pd.read_csv(errfile, sep=' = ', names=['name', 'value'], engine='python')
    params = params.dropna()
    params = params[params.name.apply(lambda x: x.startswith('.'))]  # all parameter lines start with .
    params.name = params['name'].apply(lambda x: x.lstrip('.'))
    params.index = params.name
    params.value = params['value'].apply(lambda x: float(x.split('in')[0].strip()))
    return params 

def param_from_errfile(errfile, parname):
    params = errfile_to_df(errfile)
    val = params.loc[parname].value
    return val

def params_from_simultaneous(kernelname, store, modelname):
    problem = load_problem(os.path.join(store, modelname+".py"))
    load_best(problem, os.path.join(store, modelname+".par")) # use fit result
    params = problem.model_parameters()
    shared_params = params['models'][0]
    shared_params = { key: dict(name=value.name, value=value.value) for key, value in shared_params.items()}
    param_df = pd.DataFrame(shared_params).T
    free = pd.DataFrame(params['freevars'])
    free = free.set_index(free[free.columns[0]].apply(lambda x: x.name.split()[0]))
    for column in free.columns:
        free[column] = free[column].apply(lambda x: x.value)
    free = free.T.copy()
    params_by_model = {}
    for i, model in enumerate(free.columns):
        model_df = param_df.copy()
        model_free = pd.DataFrame({'name': free[model].index.values, 'value': free[model]})
        if model == 'SAXS': # reset scale 
            model_free.at['scale', 'value'] = 1e3
        # model_df.at[model_free.index[:-1],'value'] = model_free.loc[model_free.index[:-1]]['value']
        # somehow broken - why did I need that again? 
        params_by_model[model] = model_df
    return params_by_model 
    
def plot_contributions(model_df, q, ax, color):
    "plot the entire composite model (best-fit) and its contributions"
    fit = model_dataset(model_df.to_dict()['value'], q, kernel)
    l, = ax.plot(q, fit,
                 color=color,
                 alpha=1,
                 )
    grouped_params = separate_composite_parameters(model_df)
    for label, item in grouped_params.items():
        #smearing = qsmear.smear_selection(datasets[i], kernels[label].make_kernel([data_sas['q'][model]]))
        fit = model_dataset(item.to_dict()['value'], q, kernels[label])
        ax.plot(q, fit,
                color=l.get_color(),
                alpha=1, linestyle='dashed')
    

def get_radial_data(sname, kratky=True, sub=None):
    if 'AH' in sname:
        path = f'../data_new/merged/{sname}_merged.txt'
        factor = 1
        sub_bkg = True
    elif 'SAXS' in sname:
        path = '../../LiPFO_TDMAO/LPFOTDMAOdata/avedat/20111130_0_0073_rebin_sub.dat'
        factor = 1/0.13  # divide by capillary thickness in cm
        sub_bkg = False
    radial_data = load_data(path)
    radial_data.y -= sub_bkg * np.average(radial_data.y[np.where(radial_data.x > 2.5)[0]])
    radial_data.y *= factor
    average = np.average(radial_data.y)
    radial_data.dy *= factor
    if 'SAXS' in sname:
        radial_data.dx = 4e-3*np.ones(radial_data.x.size)
        dy_magnitude = 1
        radial_data.dy = dy_magnitude*np.ones(radial_data.x.size)#/radial_data.x**2#0.001*# no
        # useful
        # error
        # bars
        # available
        #radial_data.dy = dy_magnitude*radial_data.y.copy()
        #if kratky:
        #    radial_data.dy *= radial_data.x**2#dy_magnitude*np.ones(radial_data.x.size)
    if sub is not None:
        radial_data.y -= sub
        
    if kratky:
        radial_data.y *= (radial_data.x)**2
        #radial_data.dy *= (radial_data.x)**2
        average = np.average(radial_data.y)
        radial_data.y /= average # normalize to avoid very small residuals 

    
    return radial_data, average  

def get_cylinder_background(sample_data,
                            c_cyl = 9.27,
                            cyl_store='./pfo_cylinder/pfo_12p5_cylinder',
                            kratky=True):
    """retrieve cylinder data from cyl_store for the q-range of sample_data,
    scale to c_cyl"""
    cyl_exp = load_problem(cyl_store+'.py')
    load_best(cyl_exp, cyl_store+'.par')
    cyl_adj_q = Experiment(data = sample_data, model=cyl_exp.fitness.model)
    y_cyl = cyl_adj_q.theory()
    if kratky:
        y_cyl *= sample_data.x**2
    return y_cyl*c_cyl/12.5 # scale from 12.5 to c_cyl mM
    
    


def model_dataset(params, x, kernel):
    data = empty_data1D(x)
    model = kernel.make_kernel([x])
    return call_kernel(model, params)
import sample.sample as s; sample = s.make_sample(); pfo = s.surfactant_make_lipfo(); tdmao = s.surfactant_make_tdmao();

def to_solve(x_1, *data):
    x_1 = expit(x_1)
    c_tot, alpha_1, beta, cmc_1, cmc_2 = data
    f1 = lambda x_1, beta: np.exp(beta * (1 - x_1)**2)
    f2 = lambda x_1, beta: np.exp(beta * (x_1)**2)
    delta = lambda x_1, beta, cmc_1, cmc_2: cmc_2 * f2(x_1, beta) - cmc_1 * f1(x_1, beta)
    return x_1 - (-1*(c_tot - delta(x_1, beta, cmc_1, cmc_2)) + np.sqrt((c_tot - delta(x_1, beta, cmc_1, cmc_2))**2 + 4 * delta(x_1, beta, cmc_1, cmc_2) * c_tot * alpha_1)) / (2 * delta(x_1, beta, cmc_1, cmc_2))

def x1_agg(bin_mix):
    params = (bin_mix.c_tot,
              bin_mix.x1_sample,
              bin_mix.beta,
              bin_mix.surf1.cmc,
              bin_mix.surf2.cmc
              )
    roots = fsolve(to_solve,x0=[logit(bin_mix.x1_sample)],args=params)
    x1 = expit(roots)[0]
    bin_mix.x1_agg = x1
    return x1

def to_solve_c_m_tot(m, *data):
    #m = expit(m)
    c_tot, alpha_1, beta, x_1, cmc_1, cmc_2 = data
    f1 = lambda x_1, beta: np.exp(beta * (1 - x_1)**2)
    f2 = lambda x_1, beta: np.exp(beta * (x_1)**2)
    return 1 - alpha_1 * c_tot / (c_tot + f1(x_1, beta) * cmc_1 - m) - (1 - alpha_1) * c_tot / (c_tot + f2(x_1, beta) * cmc_2 - m)


def x_free(bin_mix):
    params = (bin_mix.c_tot, 
              bin_mix.x1_sample, 
              bin_mix.beta,
              bin_mix.x1_agg,
              bin_mix.surf1.cmc,
              bin_mix.surf2.cmc,
    )
    roots = fsolve(to_solve_c_m_tot,x0=[0.05 * bin_mix.c_tot],args=params)
    c_monomers = roots[0]
    x_free = c_monomers/bin_mix.c_tot
    bin_mix.x_free = x_free
    return x_free

def monomer_conc(bin_mix):
    monomers = bin_mix.x_free * bin_mix.c_tot
    f1 = lambda x1, beta: np.exp(beta * (1-x1)**2)
    f2 = lambda x1, beta: np.exp(beta * (x1)**2)
    c_mono_1 = (monomers - f2(bin_mix.x1_agg, bin_mix.beta) *
                bin_mix.surf2.cmc) / (1 - f2(bin_mix.x1_agg, bin_mix.beta) *
                                      bin_mix.surf2.cmc/(f1(bin_mix.x1_agg, bin_mix.beta) *
                                                                 bin_mix.surf1.cmc))
    c_mono_2 = monomers - c_mono_1
    return c_mono_1, c_mono_2



sample.x1_agg = x1_agg(sample)
sample.x_free = x_free(sample)    
sample.sld_agg = s.sld_agg(sample)
phi_vesicle = s.phi_agg(sample)
cmono1, cmono2 = monomer_conc(sample) # in mol/L

radius = 10
radius_pd = 0.12 # just as thin shell 
thickness = 2.9

outer = 1.3
radial_data, average = get_radial_data("SAXS", kratky=False)
set_beam_stop(radial_data, 0.04, outer=outer)  


kernel = load_model("custom.vesicle+cylinder")
cylinder = "./pfo_cylinder/pfo_12p5_cylinder.err"
model = Model(kernel,
              scale = 1e3, # volume is in A^3, 0.1 for SAXS in 1/mm
              background = 0,
              A_sld = sample.sld_agg, # in 1e6 / A^2
              A_sld_solvent = sample.solvent.sld_x*1e4,
              A_volfraction = phi_vesicle,
              A_radius = radius,
              A_radius_pd = radius_pd,
              A_radius_pd_type = 'schulz',
              A_thickness = thickness,
              B_scale = cmono2*1e3/12.5e3*param_from_errfile(cylinder, 'scale'),
              B_sld = param_from_errfile(cylinder, 'sld'),
              B_sld_solvent = param_from_errfile(cylinder, 'sld_solvent'),
              B_radius = param_from_errfile(cylinder, 'radius'),
              B_length = param_from_errfile(cylinder, 'length'),
              )

model.A_radius.range(0.75*radius,1.25*radius) # in nm

model.A_volfraction.range(0.1*phi_vesicle, 1.5*phi_vesicle)

model.A_thickness.range(2,4.2)
model.B_scale.range(0, phi_vesicle)

def constraints():
    ves_phi, cyl_phi = M.parameters()['A_volfraction'].value, M.parameters()['B_scale'].value
    c_cyl = cyl_phi/0.345 # using monomer value
    v_avg_molecule = sample.x1_sample * sample.surf1.v_molecule + (1-sample.x1_sample)*sample.surf2.v_molecule
    c_ves = ves_phi/v_avg_molecule
    return 0 if ((c_ves+c_cyl)-sample.c_tot*c.N_A*1e-24) < 1e-3 else 1e6

cutoff = 1e-3
M = Experiment(data=radial_data, model=model, cutoff=cutoff)
problem = FitProblem(M, constraints=constraints)
problem.store = './{}_{}_{}_{:03d}'.format(store, "SAXS", "SAXS", int(sample.x1_agg*100))
