xT=0.609684183127342
store="pfo_cylinder"
import numpy as np
from bumps.names import FitProblem 
import sys
from scipy.optimize import fsolve
from scipy.special import logit, expit
import scipy.constants as c 
import pandas as pd 
sys.path.append('../../sasview-5.0.4/src')
sys.path.append('~/sasmodels/custom_models')
sys.path.append('.')
from sasmodels.data import load_data
#from bumps.names import *
from sasmodels.core import load_model
from sasmodels.bumps_model import Model, Experiment
from sasmodels.data import load_data, set_beam_stop, set_top
import os 
import bumps 
from bumps.fitproblem import load_problem
from bumps.cli import load_best
from scipy.stats import gmean

def separate_composite_parameters(par_df):
    """Split a dataframe containing parameters from a composite
model. Input is expected cleaned from leading dots and fit ranges"""
    groups = par_df.name.apply(lambda x: x.split('_')[0]).unique()
    grouped = par_df.groupby(par_df.name.apply(lambda x: x.split('_')[0]))
    separated = {}
    for key, item in grouped:
        if len(key) < 2:
            item.at['{}_scale'.format(key),'value'] *= grouped.get_group('scale').loc['scale'].value
            item.index = item.name.apply(lambda x: x.lstrip('{}_'.format(key)))
            item = item.append(grouped.get_group('background'))
            item.drop(columns=['name'], inplace=True)
            separated[key] = item
    return separated

def errfile_to_df(errfile):
    "does not work for simultaneous fits, load model instead"
    params = pd.read_csv(errfile, sep=' = ', names=['name', 'value'], engine='python')
    params = params.dropna()
    params = params[params.name.apply(lambda x: x.startswith('.'))]  # all parameter lines start with .
    params.name = params['name'].apply(lambda x: x.lstrip('.'))
    params.index = params.name
    params.value = params['value'].apply(lambda x: float(x.split('in')[0].strip()))
    return params 

def param_from_errfile(errfile, parname):
    params = errfile_to_df(errfile)
    val = params.loc[parname].value
    return val

def params_from_simultaneous(kernelname, store, modelname):
    problem = load_problem(os.path.join(store, modelname+".py"))
    load_best(problem, os.path.join(store, modelname+".par")) # use fit result
    params = problem.model_parameters()
    shared_params = params['models'][0]
    shared_params = { key: dict(name=value.name, value=value.value) for key, value in shared_params.items()}
    param_df = pd.DataFrame(shared_params).T
    free = pd.DataFrame(params['freevars'])
    free = free.set_index(free[free.columns[0]].apply(lambda x: x.name.split()[0]))
    for column in free.columns:
        free[column] = free[column].apply(lambda x: x.value)
    free = free.T.copy()
    params_by_model = {}
    for i, model in enumerate(free.columns):
        model_df = param_df.copy()
        model_free = pd.DataFrame({'name': free[model].index.values, 'value': free[model]})
        if model == 'SAXS': # reset scale 
            model_free.at['scale', 'value'] = 1e3
        # model_df.at[model_free.index[:-1],'value'] = model_free.loc[model_free.index[:-1]]['value']
        # somehow broken - why did I need that again? 
        params_by_model[model] = model_df
    return params_by_model 
    
def plot_contributions(model_df, q, ax, color):
    "plot the entire composite model (best-fit) and its contributions"
    fit = model_dataset(model_df.to_dict()['value'], q, kernel)
    l, = ax.plot(q, fit,
                 color=color,
                 alpha=1,
                 )
    grouped_params = separate_composite_parameters(model_df)
    for label, item in grouped_params.items():
        #smearing = qsmear.smear_selection(datasets[i], kernels[label].make_kernel([data_sas['q'][model]]))
        fit = model_dataset(item.to_dict()['value'], q, kernels[label])
        ax.plot(q, fit,
                color=l.get_color(),
                alpha=1, linestyle='dashed')
    

def get_radial_data(sname, kratky=True, sub=None):
    if 'AH' in sname:
        path = f'../data_new/merged/{sname}_merged.txt'
        factor = 1
        sub_bkg = True
    elif 'SAXS' in sname:
        path = '../../LiPFO_TDMAO/LPFOTDMAOdata/avedat/20111130_0_0073_rebin_sub.dat'
        factor = 1/0.13  # divide by capillary thickness in cm
        sub_bkg = False
    radial_data = load_data(path)
    radial_data.y -= sub_bkg * np.average(radial_data.y[np.where(radial_data.x > 2.5)[0]])
    radial_data.y *= factor
    average = np.average(radial_data.y)
    radial_data.dy *= factor
    if 'SAXS' in sname:
        radial_data.dx = 4e-3*np.ones(radial_data.x.size)
        dy_magnitude = 1
        radial_data.dy = dy_magnitude*np.ones(radial_data.x.size)#/radial_data.x**2#0.001*# no
        # useful
        # error
        # bars
        # available
        #radial_data.dy = dy_magnitude*radial_data.y.copy()
        #if kratky:
        #    radial_data.dy *= radial_data.x**2#dy_magnitude*np.ones(radial_data.x.size)
    if sub is not None:
        radial_data.y -= sub
        
    if kratky:
        radial_data.y *= (radial_data.x)**2
        #radial_data.dy *= (radial_data.x)**2
        average = np.average(radial_data.y)
        radial_data.y /= average # normalize to avoid very small residuals 

    
    return radial_data, average  

def get_cylinder_background(sample_data,
                            c_cyl = 9.27,
                            cyl_store='./pfo_cylinder/pfo_12p5_cylinder',
                            kratky=True):
    """retrieve cylinder data from cyl_store for the q-range of sample_data,
    scale to c_cyl"""
    cyl_exp = load_problem(cyl_store+'.py')
    load_best(cyl_exp, cyl_store+'.par')
    cyl_adj_q = Experiment(data = sample_data, model=cyl_exp.fitness.model)
    y_cyl = cyl_adj_q.theory()
    if kratky:
        y_cyl *= sample_data.x**2
    return y_cyl*c_cyl/12.5 # scale from 12.5 to c_cyl mM
    
    


def model_dataset(params, x, kernel):
    data = empty_data1D(x)
    model = kernel.make_kernel([x])
    return call_kernel(model, params)
import sample.sample as s; sample = s.make_sample(); pfo = s.surfactant_make_pfo_monomer();
c_pfo = 12.5e-3  # mol/L 
volume0 = 0.345 # nm^3 - as monomer 
phi0 = c_pfo*c.N_A*1e-24*volume0 # mol/L -> 1/L -> 1/nm^3 -> nm^3/nm^3

kernel = load_model("cylinder")

radial_data = load_data('./pfo_cylinder/pfo_12p5_merged.txt')
radial_data.y /= 0.13   # divide by SAXS capillary thickness 1.3 mm  
radial_data.dy = 0.001*np.average(radial_data.y)*np.ones(radial_data.x.size)
set_beam_stop(radial_data, 0.4, outer=14)
model = Model(kernel,
              background = 0,
              scale = 1e3*phi0, 
              sld_solvent = sample.solvent.sld_x*1e4,
              sld = pfo.sld_x(),
              radius = 0.09,
              length = 0.984,
              )

model.radius.range(0.05, np.sqrt(volume0/np.pi))
model.scale.range(0,100)
model.sld.range(sample.surf2.sld_x(),150)

def constraints():
    phi, r, sld = M.parameters()['scale'].value/1e3, M.parameters()['radius'].value, M.parameters()['sld'].value
    length = M.parameters()['length'].value
    volume = length * np.pi * r**2
    sld_solvent = M.parameters()['sld_solvent'].value
    phi_corrected = volume/volume0*phi0
    sld_corrected = sld_solvent + ((pfo.sld_x() - sld_solvent)*volume0/volume) # conservation of scattering length 
    return 0 if abs(phi_corrected - phi)/phi < 1e-2 and abs(sld - sld_corrected)/sld < 1e-2 else 1e6*(phi-phi_corrected)**2 + 1e6*(sld-sld_corrected)**2
 
M = Experiment(data=radial_data, model=model)
problem = FitProblem(M, constraints=constraints)
problem.store = store
