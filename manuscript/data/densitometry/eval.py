from __future__ import division
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use('svg')
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import matplotlib.ticker
from itertools import cycle
import numpy as np
import glob

# plot setup
plt.clf()
plt.rcdefaults()
plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=12)
plt.rc('legend', handletextpad=0.4, frameon=False, numpoints=1, borderpad=0.2, handlelength=0.3, borderaxespad=0.4, markerscale=1)
# label function
def labels(xT):
    if xT-0 < 1e-5:
        return r"LiPFO"
    elif abs(xT-1) < 1e-5:
        return r"TDMAO"
    else:
        return r"$\alpha_T = %.1f$" % xT

# include keyword for new evaluations
tag = 'newanalysis'

rhowater = 0.99715 # g/cm3
MwT = 257.46 # g/mol
MwL = 420 # g/mol
NA = 6.022e+23 # 1/mol

def Mwmix(xT,MwT,MwL):
    return xT*MwT+(1-xT)*MwL

# determine the molar concentration from the weight fraction of solute
def cmol(weightfrac,rho,Mw):
    rho = rho*1.e3 # conversion to g/L
    cmol = weightfrac*rho/Mw
    return cmol

def vmolecule(rho,rho_err,Mwmix):
    vmol = Mwmix/rho # volume per mole in cm^3/mol
    vmol_err = vmol/rho*rho_err
    v = vmol/NA*1e21 # conversion to one molecule and nm^3
    v_err = vmol_err/NA*1e21
    return v, v_err

def line(x,m):
    return m*x+1./rhowater

def resultline(x,rho):
    return (1/rho-1/rhowater)*x+1/rhowater

def line_with_intercept(x,m,t):
    return m*x+t

def perform_fit(xdata,ydata,xT,ccondition=None):
    #p,covar = curve_fit(line_with_intercept,xdata,ydata)
    Mw = Mwmix(xT,MwT,MwL)
    #cm = cmol(xdata,1/ydata,Mw)
    if ccondition is not None:
        slice = np.where(ccondition)[0]
        p,covar = curve_fit(line,xdata[slice],ydata[slice])
    else:
        p,covar = curve_fit(line,xdata,ydata)
    slope = p[0]
    #print 'slope',slope
    slope_err = np.sqrt(covar[0,0])
    intercept = 1/rhowater#p[1]
    #print 'intercept',intercept
    #intercept_err = np.sqrt(covar[1,1])
    rho = rhowater/(rhowater*slope+1)
    rhoerr = abs(rho/slope*rhowater*slope_err)
    v, verr = vmolecule(rho,rhoerr,Mw)
    # returns the surfactant density and its estimated error
    return rho, rhoerr, v,verr#+intercept_err 

plt.clf()

data = glob.glob('densities*xT*.dat')
data = sorted(data, key=lambda item: item[-9:-5])
#print data
ddict = {}

for item in data:
    daten = open('%s' % item)
    daten.readline()
    d = np.loadtxt(daten)
    #print d
    dslice = np.zeros((int(d.shape[0]/4),d.shape[1]+1)) # one column more for standard
    # deviation of the specific volume 1/rho
    #print dslice.shape
    # average measurements at the same concentration, convert to 1/rho
    # there is an outlier, the first measurement of TDMAO at the second lowest conc.
    # is reported with a higher density than the others, but on average it is still
    # not matching the general trend - ignore for now
    for n in range(dslice.shape[0]):
        #print d[(n)*4:(n+1)*4][:,0]
        dslice[n][0] = np.average(d[(n)*4:(n+1)*4][:,0]) # weight fractions
        dslice[n][1] = np.average(1./d[(n)*4:(n+1)*4][:,1]) # 1/rho
        dslice[n][2] = np.std(1./d[(n)*4:(n+1)*4][:,1]) # stdev(1/rho)
        dslice[n][3] = np.average(d[(n)*4:(n+1)*4][:,2]) # average temperature
    #print dslice
    daten.close()
    xT = float(item.rstrip('.dat').split('xT')[1])/1.e4
    print(xT)
    Mw = Mwmix(xT,MwT,MwL)
    cmoldata = cmol(dslice[:,0],1/dslice[:,1],Mw) # conc. in mol/L
    ddict[xT] = {'xs': dslice, 'cmol': cmoldata}
    if 1:#0.2 <= xT and xT <= 0.8:
        #if xT in ['00000','00359','00699','00944','01899','02851','03808','04903','05860','06835','08898']:
        cac = 1e-4#7e-5
        rho,rho_err,v, v_err = perform_fit(dslice[:,0],dslice[:,1],xT,cac)#perform_fit(dslice[:-1,0],1/dslice[:-1,1],xT)
        # repeat calculation for only the last point
        slopealt = (1./dslice[-1,1]-1./rhowater)/(dslice[-1,0]-0)
        rhoalt = rhowater/(slopealt*rhowater+1)
        print(rho, rhoalt)
    #elif xT in ['07882']:
    #    rho,rho_err = perform_fit(dslice[:-2,0],1/dslice[:-2,1],xT)
    else:
        pass
        #dslice = np.concatenate((dslice[0].reshape(1,3),dslice[2:]))
        #print dslice
        #rho,rho_err = perform_fit(dslice[:,0],1/dslice[:,1],xT)
    #print xT, rho, rho_err
    #results.write('%f\t%f\t%f\n' % (float(xT)/10000., rho, rho_err))
results = open('densities_LiPFO_freshTDMAO_%s.dat' % tag,'w')
results.write('sample\trho / g/mL\trho_err / g/mL\t vmolecule / nm^3\tvmolecule_err / nm^3 \n')
results_belowcmcL = open('densities_LiPFO_freshTDMAO_belowcmcL.dat','w')
results_belowcmcL.write('sample\trho / g/mL\trho_err / g/mL\t vmolecule / nm^3\tvmolecule_err / nm^3 \n')
results_abovecmcL = open('densities_LiPFO_freshTDMAO_abovecmcL.dat','w')
results_abovecmcL.write('sample\trho / g/mL\trho_err / g/mL\t vmolecule / nm^3\tvmolecule_err / nm^3 \n')



# plot inverse density vs. molar concentration for xT between 0.19 and 0.68 - without
# fit, because the fit is defined for xs!
cac = 1e-4
cmcL = 33.4e-3
plt.axvline(cac*1e3,color='r',linestyle='dashed',label='cac')
plt.axvline(cmcL*1e3,color='k',linestyle='dashed',label='cmc LiPFO')
plt.axvline(8,color='0.7',linestyle=':',label=r'8, 25 and 50~mM')
plt.axvline(25,color='0.7',linestyle=':')
plt.axvline(50,color='0.7',linestyle=':')
plt.xlim(xmax=55)
plt.ylim((0.993,1.003))
plt.xlabel(r"$c$ / $\frac{\mathrm{mmol}}{\mathrm{L}}$")
plt.ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")
#plt.xscale('log')
#plt.yscale('log')
markers = cycle(['x','^','s','p',(6,0,0),'o'])
for key in sorted([key for key in ddict.keys() if (key > 0.28 and key < 0.7)]):
    print(key)
    d = ddict[key]
    plt.plot(1e3*d['cmol'],d['xs'][:,1],marker=next(markers),markersize=8,mfc='none',linewidth=0,label=labels(key))
        
l = plt.legend(frameon=True)
l.get_frame().set_facecolor('#FFFFFF')
l.get_frame().set_edgecolor('#FFFFFF')
plt.savefig('vesicles_cmol.png',bbox_inches='tight')
plt.clf()
plt.close()

# 1/rho vs cmol for pure compounds
#f, (axL,ax2) = plt.subplots(2)
axL = host_subplot(111,axes_class=AA.Axes)
axT = axL.twiny() # share y axis


cac = 1e-4
cmcL = 33.4e-3
cmcT = 1.2e-4
#axL.axvline(cac*1e3)
axT.axvline(cmcT*1e3,ymin=0.9,ymax=1,color='0.7',linestyle='dashed',label='cmc')
axL.axvline(cmcL*1e3,ymin=0,ymax=0.9,color='0.7',linestyle='dashed')
#plt.xlim(xmax=55)
#plt.ylim((0.993,1.003))
axT.xaxis.tick_top()
formatter = plt.FormatStrFormatter('%.4f')
axT.yaxis.set_major_formatter(formatter)

axL.set_xlabel(r"$c(\mathrm{LiPFO})$ / $\frac{\mathrm{mmol}}{\mathrm{L}}$") 
axT.set_xlabel(r"$c(\mathrm{TDMAO})$ / $\frac{\mathrm{mmol}}{\mathrm{L}}$") 
axL.set_ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")
#axT.set_ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")
#plt.xscale('log')
#plt.yscale('log')

#for key in [0.00,1.00]:#sorted([key for key in ddict.keys() if (key > 0.18 and key < 0.7)]):
        #print key
d = ddict[1.00]
axT.plot(1e3*d['cmol'],d['xs'][:,1],marker = 's',markersize=5,mec='k',linewidth=0,mfc='none',label=labels(1.00))
d = ddict[0.00]
axL.plot(1e3*d['cmol'],d['xs'][:,1],marker = 'o',markersize=5,mec='r',mfc='none',linewidth=0,label=labels(0.00))

axL.legend(loc='center right')
#axT.legend()
#f.subplots_adjust(hspace=0)
plt.savefig('pure_cmol.png',bbox_inches='tight')
plt.clf()
plt.close()


# plot 1/rho vs xs for alpha between 0.18 and 0.7 and pure compounds
plt.ylim((0.993,1.003))
plt.xlabel(r"$x_S$ / $\frac{\mathrm{g}}{\mathrm{g}}$")
plt.ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")
#plt.xscale('log')
#plt.yscale('log')
for key in sorted([key for key in ddict.keys() if (key > 0.0 and key < 0.7)]):
        #print key
        d = ddict[key]
        abovecmc = np.where(d['cmol'] > cmcL)[0]
        belowcmc = np.where(d['cmol'] < cmcL)[0]
        res = perform_fit(d['xs'][:,0],d['xs'][:,1],key,((d['cmol'] > cac) & (d['cmol'] < 50e-3)))
        results.write('{}\t{}\t{}\t{}\t{}\n'.format(str(key),*res))
        # separate calculation only where discrepancies are visible
        if key > 0.48:
                res_below = perform_fit(d['xs'][belowcmc,0],d['xs'][belowcmc,1],key)
                res_above = perform_fit(d['xs'][abovecmc,0],d['xs'][abovecmc,1],key)

                results_belowcmcL.write('{}\t{}\t{}\t{}\t{}\n'.format(str(key),*res_below))
                results_abovecmcL.write('{}\t{}\t{}\t{}\t{}\n'.format(str(key),*res_above))
        print(key, res)
        if key > 0.28:
                plt.plot(d['xs'][:,0],resultline(d['xs'][:,0],res[0]))
                plt.plot(d['xs'][:,0],d['xs'][:,1],'o',markersize=4,mfc='none',label=labels(key)+r" $v=%.2f\pm %.2f~\mathrm{nm}^3$" % (res[2],res[3]))

plt.legend()
plt.savefig('vesicles_xs.png',bbox_inches='tight')
plt.clf()
plt.close()

# pure compounds
plt.xlabel(r"$x_S$ / $\frac{\mathrm{g}}{\mathrm{g}}$")
f, (ax1,ax2) = plt.subplots(2)

cac = 1e-4
cmcL = 33.4e-3
cmcT = 1.2e-4
#plt.xlim(xmax=55)
#plt.ylim((0.993,1.003))
ax1.xaxis.tick_top()
formatter = plt.FormatStrFormatter('%.4f')
ax2.yaxis.set_major_formatter(formatter)

ax2.set_xlabel(r"$x_S$ / $\frac{\mathrm{g}}{\mathrm{g}}$")
ax1.set_ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")
ax2.set_ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")

d = ddict[1.00]
# mask the second point here, it is a clear outlier probably due to a bubble
res = perform_fit(d['xs'][np.arange(d['xs'].shape[0])!=1,0],d['xs'][np.arange(d['xs'].shape[0])!=1,1],1.00,((d['cmol'] > cac) & (d['cmol'] < 50e-3)))
results.write('{}\t{}\t{}\t{}\t{}\n'.format(str(1.00),*res))
print('T micelle', res)
ax1.plot(d['xs'][:,0],resultline(d['xs'][:,0],res[0]),label=labels(1.00)+', $v = %.2f~\mathrm{nm}^3$' % (res[2]),color='k')
ax1.plot(d['xs'][:,0],d['xs'][:,1],marker = 's',markersize=5,mfc='none',mec='k',linewidth=0,label=labels(1.00))
d = ddict[0.00]
# fit above the cmc
res = perform_fit(d['xs'][:,0],d['xs'][:,1],0.00,(d['cmol'] > cmcL))
print('L micelle', res)
# fit below the cmc
resfree = perform_fit(d['xs'][:,0],d['xs'][:,1],0.00,(d['cmol'] < cmcL))
print('L free', resfree)
results_abovecmcL.write('{}\t{}\t{}\t{}\t{}\n'.format(str(0.00),*res))
results_belowcmcL.write('{}\t{}\t{}\t{}\t{}\n'.format(str(0.00),*resfree))
ax2.plot(d['xs'][:,0],resultline(d['xs'][:,0],resfree[0]),color='r',linestyle='dashed',label='below cmc, $v = %.2f~\mathrm{nm}^3$' % (resfree[2])) # 
ax2.plot(d['xs'][:,0],resultline(d['xs'][:,0],res[0]),color='k',label='above cmc, $v = %.2f~\mathrm{nm}^3$' % (res[2])) # micellar density

abovecmc = np.where(d['cmol'] > cmcL)[0]
belowcmc = np.where(d['cmol'] < cmcL)[0]
ax2.plot(d['xs'][belowcmc,0],d['xs'][belowcmc,1],marker = 'o',markersize=5,mfc='r',mec='r',linewidth=0,label=labels(0.00)+' below cmc')
ax2.plot(d['xs'][abovecmc,0],d['xs'][abovecmc,1],marker = 'o',markersize=5,mec='k',mfc='none',linewidth=0,label=labels(0.00)+' above cmc')



ax1.legend()
ax2.legend(loc='lower left')
f.subplots_adjust(hspace=0)


plt.savefig('pure_xs.png',bbox_inches='tight')
plt.clf()
plt.close()

results.close()
results_belowcmcL.close()
results_abovecmcL.close()


# plot inverse density vs. mixing ratio
#results = open('densities_LiPFO_freshTDMAO_%s.dat' % tag)
#results.readline()
#d = np.loadtxt(results)
#plt.ylim((0.48,1.15))
#plt.errorbar(d[:,0],1./d[:,1],yerr=d[:,2]/d[:,1]**2,fmt='o')
#plt.xlabel(r"$x_{\mathrm{TDMAO}}$")
#plt.ylabel(r"$\rho^{-1}$ / $\frac{\mathrm{mL}}{\mathrm{g}}$")
#plt.savefig('invdensity_vs_xT_%s.png' % tag)
#plt.clf()
#plt.close()
#results.close()


# plot molecular volumes vs. TDMAO content
daten = open('densities_LiPFO_freshTDMAO_%s.dat' % tag)
daten.readline()
d = np.genfromtxt(daten)#,skip_footer=1)
daten.close()
daten = open('densities_LiPFO_freshTDMAO_%s.dat' % 'belowcmcL')
daten.readline()
dbelow = np.genfromtxt(daten)#,skip_footer=1)
daten.close()
daten = open('densities_LiPFO_freshTDMAO_%s.dat' % 'abovecmcL')
daten.readline()
dabove = np.genfromtxt(daten)#,skip_footer=1)
daten.close()
xTs = np.linspace(0,1,10)
vesicles = np.where((d[:,0] > 0.28) & (d[:,0] < 0.7))[0]
p,covar = curve_fit(line_with_intercept,d[vesicles,0],d[vesicles,3],sigma=d[vesicles,4])
plt.errorbar(d[:,0],d[:,3],yerr=d[:,4],fmt='^',color='b',capsize=2)
vL = p[1] # intercept for xT=0
vLerr = np.sqrt(covar[1,1])
vT = p[1]+1*p[0] # for xT = 1
vTerr = vLerr+np.sqrt(covar[0,0])
plt.plot(xTs,line_with_intercept(xTs,p[0],p[1]),color='b',label=r'all $v_T = (%.3f\pm%.3f)~\mathrm{nm}^3, v_L = (%.3f\pm%.3f)~\mathrm{nm}^3$' % (vT,vTerr,vL,vLerr))
# below cmc for xT >= 0.49
lowxT = d[vesicles]
lowxT = lowxT[np.where(lowxT[:,0] < 0.48)[0]]
print(lowxT)
db = np.concatenate((lowxT,dbelow[np.where(dbelow[:,0] > 0)[0]])) # does only contain vesicle data except for LiPFO
print(db)

pb,covarb = curve_fit(line_with_intercept,db[:,0],db[:,3],sigma=db[:,4])
plt.errorbar(dbelow[:,0],dbelow[:,3],yerr=dbelow[:,4],fmt='o',color='r',capsize=2,label='below the cmc of LiPFO')
vLb = pb[1] # intercept for xT=0
vLerr = np.sqrt(covarb[1,1])
vTb = pb[1]+1*pb[0] # for xT = 1
vTerr = vLerr+np.sqrt(covarb[0,0])
plt.plot(xTs,line_with_intercept(xTs,pb[0],pb[1]),'r',label=r'below cmcL, $v_T = (%.3f\pm%.3f)~\mathrm{nm}^3, v_L = (%.3f\pm%.3f)~\mathrm{nm}^3$' % (vTb,vTerr,vLb,vLerr))
# above cmc only - one point, error bars not possible
da = np.concatenate((lowxT,dabove[np.where(dbelow[:,0] > 0)[0]]))
print(da)
pa,covara = curve_fit(line_with_intercept,da[:,0],da[:,3])#,sigma=da[:,4])
plt.errorbar(dabove[:,0],dabove[:,3],yerr=dabove[:,4],fmt='o',ecolor='k',capsize=2,mec='k',mfc='none',label='above the cmc of LiPFO')
vLa = pa[1] # intercept for xT=0
vLerr = np.sqrt(covara[1,1])
vTa = pa[1]+1*pa[0] # for xT = 1
vTerr = vLerr+np.sqrt(covara[0,0])
plt.plot(xTs,line_with_intercept(xTs,pa[0],pa[1]),'k',label=r'above cmcL $v_T = (%.3f\pm%.3f)~\mathrm{nm}^3, v_L = (%.3f\pm%.3f)~\mathrm{nm}^3$' % (vTa,vTerr,vLa,vLerr))


#print 'molecule volume TDMAO = ', p[0]+p[1], ', LiPFO = ',p[1]
plt.ylim(ymax=0.525)
legend = plt.legend(loc='upper left',frameon=True)
legend.get_frame().set_facecolor('#FFFFFF')
legend.get_frame().set_edgecolor('#FFFFFF')
legend.get_frame().set_alpha(0.5)

plt.xlabel(r"$x_{\mathrm{TDMAO}}$")
plt.ylabel(r"$V_{\mathrm{molecule}}$ / $\mathrm{nm}^3$")
plt.savefig('molecvolume_vs_xT_%s.svg' % tag)
plt.clf()
plt.close()






# Explanation for formula:
# V = Vsurf + Vw
# m/rho = msurf/rhosurf + mw/rhow
# rho = m/(msurf/rhosurf + mw/rhow)
# rho = 1/(msurf/m*1/rhosurf + mw/m*1/rhow)
# rho = 1/(wt%surf/rhosurf + (1-wt%surf)/rhow)
# rho = 1/((wt%surf*rhow+rhosurf-wt%surf*rhosurf)/(rhosurf*rhow))
# 1/rho = 1/rhow + wt%surf*(rhow-rhosurf)/(rhosurf*rhow)
# intercept of 1/rho: 1/rhow
# slope of 1/rho: m = (rhow-rhosurf)/(rhosurf*rhow)
# ->  1/rhosurf = (m*rhow+1)/rhow <-> rhosurf = rhow/(m*rhow+1)


