xT=0.609684183127342
import numpy as np 
from scipy.stats import rv_continuous

class mixture(rv_continuous):
    
        
    def _pdf(self, x, x_avg, r, ro, factor):
        def x_i(x, x_avg, r, ro):
            return x_avg * (r**2 + ro**2)/r**2 - x*ro**2/r**2

        def frac_o(r, ro):
            return ro**2/(r**2 + ro**2)

        return factor*np.exp(-(frac_o(r,ro) * (x*np.log(x) +
                                        (1-x)*np.log(1-x)) +
                        (1-frac_o(r,ro)) * (x_i(x, x_avg, r, ro) * np.log(x_i(x, x_avg, r, ro))
                                            + (1-x_i(x, x_avg, r, ro))*np.log(1-x_i(x, x_avg, r, ro)))))
    
    def _get_support(self, x_avg, r, ro, factor):
        a = max(0, r**2/ro**2*(x_avg*(ro**2+r**2)/r**2 - 1))
        b = min(x_avg*(ro**2+r**2)/ro**2, 1)
        return a, b 

factor = 1
