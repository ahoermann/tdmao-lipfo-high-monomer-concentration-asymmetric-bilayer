xT=0.609681328920215
from glob import glob
import numpy as np
import scipy.constants as c
import pandas as pd
import lmfit as lm
import matplotlib.pyplot as plt 

def g2_cum2(x, beta, gamma, mu2):
    return beta * np.exp(-2*gamma*x) * (1+mu2/2.*x**2)**2

g2_fit_cum = lm.Model(g2_cum2)
g2_fit_cum.set_param_hint('beta', min=0, value=0.3, max=1)
g2_fit_cum.set_param_hint('Gamma', min=0, value=2)
g2_fit_cum.set_param_hint('mu2', min=0, value=0.2)

def guinier(q, rg, i0):
    return i0 * np.exp(-rg**2/3. * q**2)

guinier_fit = lm.Model(guinier) 
guinier_fit.set_param_hint('i0', min=0, value=1e-3)
guinier_fit.set_param_hint('rg', min=0, value=30)  # load guinier fit model
import scipy.constants as c
import numpy as np

def q(angle, wavel, solvent):
    "calculate the scattering vector (1/nm) from the angle (deg) and the wavelength (nm) as well as solvent properties (in SI units)"
    return c.pi * 4 * solvent.ref_index /wavel * np.sin(angle/2.*c.pi/180.)

def rh(gamma, q, solvent, temp):
    "calculate the hydrodyn. radius (nm) from gamma (1/ms), q (1/nm), temperature (K)" 
    diff_coeff = gamma/q**2 * 1e-15  # nm^2/ms * m2/nm^2 * ms/s
    return c.k * temp /(6 * c.pi * solvent.eta * diff_coeff) * 1e9 # unit: J/(J/m^3 * m^2/s) * 1e9 -> nm
  # q, rh,
import attr

@attr.s
class Surfactant(object):
    name = attr.ib(type=str,default='lipfo')
    molweight = attr.ib(type=float,default=339.99, metadata={'unit': 'g/mol'})
    cmc = attr.ib(type=float,default=33.4e-3, metadata={'unit': 'mol/L'})
    c_monomer = attr.ib(type=float, default=0, metadata={'unit': 'mol/L'})
    v_molecule = attr.ib(type=float,default=0.5409, metadata={'unit': 'nm^3'})
    sl_x = attr.ib(type=float,default=1, metadata={'unit': '1e-4 nm'})
    sl_n = attr.ib(type=float,default=1, metadata={'unit': '1e-4 nm'})
    def sld_x(self):
        return self.sl_x/self.v_molecule
    def sld_n(self):
        return self.sl_n/self.v_molecule

@attr.s
class Solvent(object):
    name = attr.ib(type=str,default='water')
    sld_x = attr.ib(type=float,default=9.414e-4, metadata={'unit': 'nm^-2'})
    sld_n = attr.ib(type=float,default=-0.5589058467419881e-4, metadata={'unit': 'nm^-2'}) 
    rho = attr.ib(type=float,default=0.99712)
    eta = attr.ib(type=float, default=0.89e-3, metadata={'unit': 'Pa.s'}) 
    ref_index = attr.ib(type=float,default=1.33)
    epsilon_r = attr.ib(type=float,default=78)
    molweight = attr.ib(type=float, default=18.02)

@attr.s
class BinaryMixture(object):
    surf1 = attr.ib(type=object)
    surf2 = attr.ib(type=object)
    solvent = attr.ib(type=object)
    x1_sample = attr.ib(type=float, default=3/7.)
    x1_agg = attr.ib(type=float, default=x1_sample)
    c_tot = attr.ib(type=float, default=0.025, metadata={'unit': 'mol/L'})
    x_free = attr.ib(type=float, default=0)
    phi_agg = attr.ib(type=float, default=0)
    sld_agg = attr.ib(type=float, default=0)
    beta = attr.ib(type=float, default=-12.0)
    a12 = attr.ib(type=float, default=-5.3)
    a21 = attr.ib(type=float, default=-6.9)

water = Solvent() 
conc_dict = {'AH283': 4,
                 'AH282': 8,
                 'AH281': 12.5,
                 'AH280': 25,
                 'AH279': 37.5,
                 }

fig_sls, ax_sls = plt.subplots()
fig_dls, ax_dls = plt.subplots()
radius_dict = {}
for pklfile in glob('./ls/AH*.pkl'):
    print(pklfile)
    df = pd.read_pickle(pklfile)
    samplename = pklfile.split('/')[-1].split('.')[0]
    print(samplename)
    conc = conc_dict[samplename]

    r, = ax_dls.plot(q(df.loc['angle'], 632.8, water)**2,
                     rh(df.loc['gamma'], q(df.loc['angle'], 632.8, water), water, 298),
                     '.', 
                     label=str(conc)+' mM')

    by_angle = df.T.groupby('angle').agg('max')

    ax_dls.plot(q(by_angle[50.0:].index, 632.8, water)**2,
                rh(by_angle[50.0:]['gamma'], q(by_angle[50.0:].index, 632.8, water), water, 298),
                'o', color = r.get_color(),
                )
    rh_avg = np.max(rh(by_angle[50.0:]['gamma'].max(), q(by_angle[50.0:].index, 632.8, water), water, 298))

    rg_fit = guinier_fit.fit(by_angle[55.0:120.0]['intensity'],
                             q = q(by_angle[55.0:120.0].index, 632.8, water),
                             i0 = 1e-2, rg = 30)
    # fit not working, no data selected - probably because fitted again with plot 

    i, = ax_sls.plot(q(by_angle[30.0:].index, 632.8, water), by_angle[30.0:]['intensity'], 'o', label=str(conc)+' mM')
    ax_sls.plot(q(by_angle[55.0:].index, 632.8, water),
                rg_fit.best_values['i0']*np.exp(-rg_fit.best_values['rg']**2/3. * q(by_angle[55.0:].index, 632.8, water)**2),
                label=r"$R_{\mathrm{g}} = $"+'{:.2f} nm'.format(rg_fit.best_values['rg']),
                color = i.get_color())
    radius_dict[samplename] = {'Rh': rh_avg, 'Rg': rg_fit.best_values['rg'], 'conc': conc_dict[samplename]}
    ax_dls.hlines([rh_avg],xmin=q(50, 632.8, water)**2,xmax=q(150, 632.8, water)**2, color = r.get_color(), label=r"$R_{\mathrm{h}} = $"+'{:.2f} nm'.format(rh_avg))

pd.DataFrame(radius_dict).T.to_csv('ls_concentration.csv')

plt.figure(fig_sls)
plt.xlabel(r"$q$ / $\mathrm{nm}^{-1}$")
plt.ylabel(r"$I$ / cm$^{-1}$")
plt.loglog()
plt.legend()
plt.savefig('./ls/concentration_series_sls.png')
plt.clf()
plt.close()

plt.figure(fig_dls)
plt.xlabel(r"$q^{2}$ / $\mathrm{nm}^{-2}$")
plt.ylabel(r"$R_{\mathrm{h}}$ / nm")
plt.ylim(ymin=20)
plt.legend()
plt.savefig('./ls/concentration_series_dls.png')
plt.clf()
plt.close()
