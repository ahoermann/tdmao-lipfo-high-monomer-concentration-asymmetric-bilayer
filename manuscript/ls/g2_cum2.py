xT=0.609681328920215
import scipy.constants as c
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import re
import itertools 
from io import StringIO
import mmap
import time 
import lmfit as lm

import attr

@attr.s
class Surfactant(object):
    name = attr.ib(type=str,default='lipfo')
    molweight = attr.ib(type=float,default=339.99, metadata={'unit': 'g/mol'})
    cmc = attr.ib(type=float,default=33.4e-3, metadata={'unit': 'mol/L'})
    c_monomer = attr.ib(type=float, default=0, metadata={'unit': 'mol/L'})
    v_molecule = attr.ib(type=float,default=0.5409, metadata={'unit': 'nm^3'})
    sl_x = attr.ib(type=float,default=1, metadata={'unit': '1e-4 nm'})
    sl_n = attr.ib(type=float,default=1, metadata={'unit': '1e-4 nm'})
    def sld_x(self):
        return self.sl_x/self.v_molecule
    def sld_n(self):
        return self.sl_n/self.v_molecule

@attr.s
class Solvent(object):
    name = attr.ib(type=str,default='water')
    sld_x = attr.ib(type=float,default=9.414e-4, metadata={'unit': 'nm^-2'})
    sld_n = attr.ib(type=float,default=-0.5589058467419881e-4, metadata={'unit': 'nm^-2'}) 
    rho = attr.ib(type=float,default=0.99712)
    eta = attr.ib(type=float, default=0.89e-3, metadata={'unit': 'Pa.s'}) 
    ref_index = attr.ib(type=float,default=1.33)
    epsilon_r = attr.ib(type=float,default=78)
    molweight = attr.ib(type=float, default=18.02)

@attr.s
class BinaryMixture(object):
    surf1 = attr.ib(type=object)
    surf2 = attr.ib(type=object)
    solvent = attr.ib(type=object)
    x1_sample = attr.ib(type=float, default=3/7.)
    x1_agg = attr.ib(type=float, default=x1_sample)
    c_tot = attr.ib(type=float, default=0.025, metadata={'unit': 'mol/L'})
    x_free = attr.ib(type=float, default=0)
    phi_agg = attr.ib(type=float, default=0)
    sld_agg = attr.ib(type=float, default=0)
    beta = attr.ib(type=float, default=-12.0)
    a12 = attr.ib(type=float, default=-5.3)
    a21 = attr.ib(type=float, default=-6.9)
import sample.sample as s; sample = s.make_sample(); 

cr_tol = pd.read_pickle('./ls/toluene_cr_quartz.pkl')
n_toluene = 1.49
r_toluene = 1.34e-5 # 1/cm
cr_solv = pd.read_pickle('./ls/h2o_cr_quartz.pkl')
n_water = 1.33

datapath_ls = '../DLS_new/'
data_by_sample = {r'AH279': {'deltat0': 14*24*60*60,
                             'loc_name_pair':
                             [('../DLS_new/20210621',
                               'AH279'),
                              ],
                             },
                  r'AH280': {'deltat0': 14*24*60*60,
                             'loc_name_pair':
                             [('../DLS_new/20210621',
                               'AH280'),
                              ],
                             },
                  r'AH281': {'deltat0': 14*24*60*60,
                             'loc_name_pair':
                             [('../DLS_new/20210621',
                               'AH281'),
                              ],
                             },
                  r'AH282': {'deltat0': 14*24*60*60,
                             'loc_name_pair':
                             [('../DLS_new/20210621',
                               'AH282'),
                              ],
                             },
                  r'AH283': {'deltat0': 14*24*60*60,
                             'loc_name_pair':
                             [('../DLS_new/20210621',
                               'AH283'),
                              ],
                             },

                  }

# list of filenames for each sample 
for label, item in data_by_sample.items():
     data_by_sample[label]['files'] = []
     data_by_sample[label]['numbers'] = []
     for path, sname in item['loc_name_pair']:
          for dirpath, dirnames, filenames in os.walk(path):
               samplefiles = list(map(lambda f: os.path.join(dirpath, f), filter(lambda x: sname in x, filenames)))
               data_by_sample[label]['files'].extend(samplefiles)

def g2_cum2(x, beta, gamma, mu2):
    return beta * np.exp(-2*gamma*x) * (1+mu2/2.*x**2)**2

g2_fit_cum = lm.Model(g2_cum2)
g2_fit_cum.set_param_hint('beta', min=0, value=0.3, max=1)
g2_fit_cum.set_param_hint('Gamma', min=0, value=2)
g2_fit_cum.set_param_hint('mu2', min=0, value=0.2)

def guinier(q, rg, i0):
    return i0 * np.exp(-rg**2/3. * q**2)

guinier_fit = lm.Model(guinier) 
guinier_fit.set_param_hint('i0', min=0, value=1e-3)
guinier_fit.set_param_hint('rg', min=0, value=30)
pal = sns.color_palette('muted', n_colors = 4)
angle_colors = lambda angle: pal[int((angle - 60)/30)]
markers = itertools.cycle(['o', 'x', 'v'])


for label, item in data_by_sample.items():
    sampledata = {}
    g2fig, (g2ax,residax) = plt.subplots(2,1,sharex=True)
    plt.xscale('log')
    for filename in item['files']:
        filedata = {}
        with open(filename, encoding="latin-1") as f_open:
            corr = mmap.mmap(f_open.fileno(), 0, access=mmap.ACCESS_READ)
            meas_date = corr[corr.find(b'Date'):].decode(encoding='latin-1').split('"')[1]
            meas_time = corr[corr.find(b'Time'):].decode(encoding='latin-1').split('"')[1]
            try: 
                tstamp = time.strptime('{:s} {:s}'.format(meas_date, meas_time), '%d.%m.%Y %H:%M:%S')
            except ValueError:
                tstamp = time.strptime('{:s} {:s}'.format(meas_date, meas_time), '%d/%m/%Y %H:%M:%S')
            angle = float(corr[corr.find(b'Angle'):corr.find(b'Duration')].decode(encoding='latin-1').split(':')[1])
            duration = float(corr[corr.find(b'Duration'):corr.find(b'FloatDur')].decode(encoding='latin-1').split(':')[1])
            if angle in cr_tol.keys():
                number = meas_date+re.search('_([\d]{1,4})\.ASC', filename).group(1)
                g2_slice = corr[corr.find(b'Correlation'):corr.find(b'Count Rate')-1].decode(encoding='latin-1')
                corrdata = pd.read_csv(StringIO(g2_slice), sep='\t',
                                       header=1, skipfooter=1, engine='python',
                                       usecols=[0,1,2],
                                       names=['tau', 'CR0/1', 'CR1/0'], 
                                       dtype=float) 
                cr_slice = corr[corr.find(b'Count Rate'):corr.find(b'Monitor Diode')-1].decode(encoding='latin-1')
                crdata = pd.read_csv(StringIO(cr_slice), sep='\t', header=1, 
                                     names=['time', 'CR0', 'CR1', 'CR2', 'CR3'], 
                                     dtype=float) 
                monitor = float(re.search(b'[\d\.]+', corr[corr.find(b'Monitor Diode'):]).group())
                crdata['time'] += time.mktime(tstamp) 
                crdata['intensity'] = (0.5*(crdata['CR0']+crdata['CR1'])-cr_solv[angle])*r_toluene/(cr_tol[angle]*monitor)
                
        g2_fit_cum = lm.Model(g2_cum2)
        g2_fit_cum.set_param_hint('beta', min=0, value=0.3, max=1)
        g2_fit_cum.set_param_hint('Gamma', min=0, value=0.1)
        g2_fit_cum.set_param_hint('mu2', min=0, value=1)
        g2_res = g2_fit_cum.fit(0.5*(corrdata['CR0/1']+corrdata['CR1/0']), x=corrdata['tau'],
                                beta=0.3, gamma=0.04)
        if angle in [90.0, 150.0]:
            marker = next(markers)
            color = angle_colors(angle) 
            g, = g2ax.plot(corrdata['tau'],
                           g2_cum2(corrdata['tau'], g2_res.best_values['beta'], g2_res.best_values['gamma'], g2_res.best_values['mu2']),
                           color = color
                           )
            g2ax.plot(corrdata['tau'],
                      0.5*(corrdata['CR0/1']+corrdata['CR1/0']),
                      marker = marker, markersize=4, mfc='none', color = g.get_color(), label=angle)
            residax.plot(corrdata['tau'],
                         0.5*(corrdata['CR0/1']+corrdata['CR1/0']) - g2_cum2(corrdata['tau'], g2_res.best_values['beta'], g2_res.best_values['gamma'], g2_res.best_values['mu2']),
                         marker = marker, mfc='none', color = g.get_color())
        filedata['angle'] = angle 
        filedata['beta'] = g2_res.best_values['beta']
        filedata['gamma'] = g2_res.best_values['gamma']
        filedata['mu2'] = g2_res.best_values['mu2']
        filedata['intensity'] = crdata['intensity'].mean()
        sampledata[number] = filedata
        # per file: measurement parameters, [gamma, beta, mu2], intensity -> pkl per sample 
    sample_df = pd.DataFrame(sampledata)
    sample_df.to_pickle('./ls/{}.pkl'.format(label))
    g2ax.legend()
    plt.xlabel(r'$\tau$ / ms')
    g2ax.set_ylabel(r'$g^{(2)}(\tau) - 1$')
    residax.set_ylabel('residuals')
    plt.savefig('./ls/g2_cum2_{}.png'.format(label), bbox_inches='tight')
    plt.clf()
    plt.close()
