xT=0.609684183127342
import scipy.constants as c
import periodictable as pt
import attr

@attr.s
class Surfactant(object):
    name = attr.ib(type=str,default='lipfo')
    molweight = attr.ib(type=float,default=339.99, metadata={'unit': 'g/mol'})
    cmc = attr.ib(type=float,default=33.4e-3, metadata={'unit': 'mol/L'})
    c_monomer = attr.ib(type=float, default=0, metadata={'unit': 'mol/L'})
    v_molecule = attr.ib(type=float,default=0.5409, metadata={'unit': 'nm^3'})
    sl_x = attr.ib(type=float,default=1, metadata={'unit': '1e-4 nm'})
    sl_n = attr.ib(type=float,default=1, metadata={'unit': '1e-4 nm'})
    def sld_x(self):
        return self.sl_x/self.v_molecule
    def sld_n(self):
        return self.sl_n/self.v_molecule

@attr.s
class Solvent(object):
    name = attr.ib(type=str,default='water')
    sld_x = attr.ib(type=float,default=9.414e-4, metadata={'unit': 'nm^-2'})
    sld_n = attr.ib(type=float,default=-0.5589058467419881e-4, metadata={'unit': 'nm^-2'}) 
    rho = attr.ib(type=float,default=0.99712)
    eta = attr.ib(type=float, default=0.89e-3, metadata={'unit': 'Pa.s'}) 
    ref_index = attr.ib(type=float,default=1.33)
    epsilon_r = attr.ib(type=float,default=78)
    molweight = attr.ib(type=float, default=18.02)

@attr.s
class BinaryMixture(object):
    surf1 = attr.ib(type=object)
    surf2 = attr.ib(type=object)
    solvent = attr.ib(type=object)
    x1_sample = attr.ib(type=float, default=3/7.)
    x1_agg = attr.ib(type=float, default=x1_sample)
    c_tot = attr.ib(type=float, default=0.025, metadata={'unit': 'mol/L'})
    x_free = attr.ib(type=float, default=0)
    phi_agg = attr.ib(type=float, default=0)
    sld_agg = attr.ib(type=float, default=0)
    beta = attr.ib(type=float, default=-12.0)
    a12 = attr.ib(type=float, default=-5.3)
    a21 = attr.ib(type=float, default=-6.9)

def surfactant_make_tdmao():
    v_molecule = 0.482 # in mixed micelle 
    tdmao = pt.formula("C16H35NO")
    tdmao.natural_density = tdmao.molecular_mass/(v_molecule*1e-21) # g/cm3
    sl_x = pt.xray_sld(tdmao, energy=12)[0]*v_molecule # 1e-4 nm
    sl_n = pt.neutron_sld(tdmao, wavelength=5)[0]*v_molecule # 1e-4 nm
    return Surfactant(name = 'tdmao',
                      molweight = tdmao.mass,
                      cmc = 0.000145,
                      v_molecule = v_molecule,  
                      sl_x = sl_x,
                      sl_n = sl_n,
                      )

tdmao = surfactant_make_tdmao()

def surfactant_make_lipfo(x_diss=0.2):
    "surfactant object with properties as LiPFO in the micelle"
    v_molecule = 0.356 # dissociated, in mixed micelle 
    pfo = pt.formula("C8F15OO{-}")
    pfo.natural_density = pfo.molecular_mass/(v_molecule*1e-21) # g/cm3
    v_molecule_assoc = 0.337
    lipfo = pt.formula("C8F15O2Li")
    lipfo.natural_density = lipfo.molecular_mass/(v_molecule_assoc*1e-21) # g/cm3
    massfraction_pfo = (x_diss*pfo.mass)/(x_diss*pfo.mass + (1-x_diss)*lipfo.mass)
    pfo_micelle = pt.mix_by_weight(pfo, massfraction_pfo, lipfo, 1-massfraction_pfo)
    v_pfo_micelle = 1e21*pfo_micelle.molecular_mass/(pfo_micelle.density*5) # factor
                                                                       # 5:
                                                                       # mixed
                                                                       # molecule
                                                                       # has
                                                                       # 4
                                                                       # PFO-
                                                                       # and
                                                                       # 1
                                                                       # LiPFO
    sl_x = pt.xray_sld(pfo_micelle, energy=12)[0]*v_pfo_micelle # 1e-4 nm
    sl_n = pt.neutron_sld(pfo_micelle, wavelength=5)[0]*v_pfo_micelle # 1e-4 nm
    return Surfactant(name = 'pfo',
                      molweight = pfo_micelle.mass/5.,
                      cmc = 33.4e-3,
                      v_molecule = v_pfo_micelle,  
                      sl_x = sl_x,
                      sl_n = sl_n,
                      )




pfo = surfactant_make_lipfo()
h2o = pt.formula("H2O", natural_density = 0.997)
li_ion = pt.formula("Li{+}")
li_ion.natural_density = li_ion.molecular_mass/(-0.019e-21)
bkg = pt.mix_by_weight(li_ion, 4/7.*25/(55.5e3), h2o, 1-4/7.*25/(55.5e3))
water = Solvent(rho = h2o.density,
                molweight = h2o.mass,
                sld_x = pt.xray_sld(h2o, energy=12)[0],
                sld_n = pt.neutron_sld(h2o, wavelength=5)[0])
d2o = pt.formula("D2O", natural_density = 0.997)
phi_h2o_in_d2o = 0.03 # experimentally, about 3% v/v of d2o is
                      # exchanged with h2o from the air
d2o = pt.mix_by_volume(d2o, 1-phi_h2o_in_d2o, h2o, phi_h2o_in_d2o)

heavy_water = Solvent(name = 'heavy_water',
                      rho = d2o.density,
                      eta = 1.0939e-3,#1.25e-3, Kestin et al 
                      ref_index = 1.328,
                      molweight = d2o.mass,
                      sld_n = pt.neutron_sld(d2o, wavelength=5)[0]
                      )

def make_sample():
    return BinaryMixture(
    surf1 = surfactant_make_tdmao(),
    surf2 = surfactant_make_lipfo(),
    solvent = Solvent(),
    )

sample = make_sample()

def surfactant_make_pfo_monomer():
    v_molecule = 0.345 # dissociated, in solution
    pfo = pt.formula("C8F15OO{-}")
    pfo.natural_density = pfo.molecular_mass/(v_molecule*1e-21) # g/cm3
    sl_x = pt.xray_sld(pfo, energy=12)[0]*v_molecule # 1e-4 nm
    sl_n = pt.neutron_sld(pfo, wavelength=5)[0]*v_molecule # 1e-4 nm
    pfo = Surfactant(name = 'pfo',
                      molweight = pfo.mass,
                      cmc = 33.4e-3,
                      v_molecule = v_molecule,  
                      sl_x = sl_x,
                      sl_n = sl_n,
                      )
    return pfo 


def background_pfo(c_pfo, sample, phi_ves, radiation='x'):
    pfo = surfactant_make_pfo_monomer()
    v_avg_molecule = sample.x1_sample * sample.surf1.v_molecule + (1-sample.x1_sample)*sample.surf2.v_molecule
    phi_surfactant = sample.c_tot*c.N_A*1e-24*v_avg_molecule
    phi_pfo = phi_surfactant - phi_ves  # free TDMAO negligible
    if radiation == "x": 
        sld_pfo = pfo.sl_x/pfo.v_molecule  # nm^-2
        scale_bkg = 1e7 # SAXS data scaled to 1/cm upon loading 
    elif radiation == "n":
        sld_pfo = pfo.sl_n/pfo.v_molecule # nm^-2
        scale_bkg = 1e7
    else:
        raise ValueError("radiation must be one of 'x', 'n'")
    background = phi_pfo*pfo.v_molecule*(sld_pfo*1e-4-sample.solvent.sld_x)**2*scale_bkg
    return background 


def phi_agg(bin_mix):
    c_agg = bin_mix.c_tot * (1-bin_mix.x_free)
    phi_agg = (bin_mix.surf1.v_molecule * bin_mix.x1_agg +
               (1-bin_mix.x1_agg) * bin_mix.surf2.v_molecule) * c_agg * 1e-24 * c.N_A
    return phi_agg

def sld_agg(bin_mix, radiation='x'):
    if radiation == 'x':
        sld1 = bin_mix.surf1.sld_x()
        sld2 = bin_mix.surf2.sld_x()
    elif radiation == 'n':
        sld1 = bin_mix.surf1.sld_n()
        sld2 = bin_mix.surf2.sld_n()
    else:
        raise ValueError("radiation must be one of 'x', 'n'")
    sld = (bin_mix.x1_agg * sld1 * bin_mix.surf1.v_molecule                 
               + (1-bin_mix.x1_agg) * sld2 *
           bin_mix.surf2.v_molecule) / ( bin_mix.x1_agg *
                                         bin_mix.surf1.v_molecule
                                         + (1-bin_mix.x1_agg) *
                                         bin_mix.surf2.v_molecule)
    return sld
