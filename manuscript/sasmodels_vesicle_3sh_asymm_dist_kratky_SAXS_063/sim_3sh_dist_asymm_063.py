xT=0.63
scale="kratky"
import numpy as np
import scipy.constants as c
from bumps.names import FitProblem
import sys
from scipy.optimize import fsolve
from scipy.special import logit, expit
import scipy.constants as c 
import pandas as pd 
sys.path.append('../../sasview-5.0.4/src')
sys.path.append('~/sasmodels/custom_models')
sys.path.append('.')
from sasmodels.data import load_data
#from bumps.names import *
from sasmodels.core import load_model
from sasmodels.bumps_model import Model, Experiment
from sasmodels.data import load_data, set_beam_stop, set_top
import os 
import bumps 
from bumps.fitproblem import load_problem
from bumps.cli import load_best
from scipy.stats import gmean

def separate_composite_parameters(par_df):
    """Split a dataframe containing parameters from a composite
model. Input is expected cleaned from leading dots and fit ranges"""
    groups = par_df.name.apply(lambda x: x.split('_')[0]).unique()
    grouped = par_df.groupby(par_df.name.apply(lambda x: x.split('_')[0]))
    separated = {}
    for key, item in grouped:
        if len(key) < 2:
            item.at['{}_scale'.format(key),'value'] *= grouped.get_group('scale').loc['scale'].value
            item.index = item.name.apply(lambda x: x.lstrip('{}_'.format(key)))
            item = item.append(grouped.get_group('background'))
            item.drop(columns=['name'], inplace=True)
            separated[key] = item
    return separated

def errfile_to_df(errfile):
    "does not work for simultaneous fits, load model instead"
    params = pd.read_csv(errfile, sep=' = ', names=['name', 'value'], engine='python')
    params = params.dropna()
    params = params[params.name.apply(lambda x: x.startswith('.'))]  # all parameter lines start with .
    params.name = params['name'].apply(lambda x: x.lstrip('.'))
    params.index = params.name
    params.value = params['value'].apply(lambda x: float(x.split('in')[0].strip()))
    return params 

def param_from_errfile(errfile, parname):
    params = errfile_to_df(errfile)
    val = params.loc[parname].value
    return val

def params_from_simultaneous(kernelname, store, modelname):
    problem = load_problem(os.path.join(store, modelname+".py"))
    load_best(problem, os.path.join(store, modelname+".par")) # use fit result
    params = problem.model_parameters()
    shared_params = params['models'][0]
    shared_params = { key: dict(name=value.name, value=value.value) for key, value in shared_params.items()}
    param_df = pd.DataFrame(shared_params).T
    free = pd.DataFrame(params['freevars'])
    free = free.set_index(free[free.columns[0]].apply(lambda x: x.name.split()[0]))
    for column in free.columns:
        free[column] = free[column].apply(lambda x: x.value)
    free = free.T.copy()
    params_by_model = {}
    for i, model in enumerate(free.columns):
        model_df = param_df.copy()
        model_free = pd.DataFrame({'name': free[model].index.values, 'value': free[model]})
        if model == 'SAXS': # reset scale 
            model_free.at['scale', 'value'] = 1e3
        # model_df.at[model_free.index[:-1],'value'] = model_free.loc[model_free.index[:-1]]['value']
        # somehow broken - why did I need that again? 
        params_by_model[model] = model_df
    return params_by_model 
    
def plot_contributions(model_df, q, ax, color):
    "plot the entire composite model (best-fit) and its contributions"
    fit = model_dataset(model_df.to_dict()['value'], q, kernel)
    l, = ax.plot(q, fit,
                 color=color,
                 alpha=1,
                 )
    grouped_params = separate_composite_parameters(model_df)
    for label, item in grouped_params.items():
        #smearing = qsmear.smear_selection(datasets[i], kernels[label].make_kernel([data_sas['q'][model]]))
        fit = model_dataset(item.to_dict()['value'], q, kernels[label])
        ax.plot(q, fit,
                color=l.get_color(),
                alpha=1, linestyle='dashed')
    

def get_radial_data(sname, kratky=True, sub=None):
    if 'AH' in sname:
        path = f'../data_new/merged/{sname}_merged.txt'
        factor = 1
        sub_bkg = True
    elif 'SAXS' in sname:
        path = '../../LiPFO_TDMAO/LPFOTDMAOdata/avedat/20111130_0_0073_rebin_sub.dat'
        factor = 1/0.13  # divide by capillary thickness in cm
        sub_bkg = False
    radial_data = load_data(path)
    radial_data.y -= sub_bkg * np.average(radial_data.y[np.where(radial_data.x > 2.5)[0]])
    radial_data.y *= factor
    average = np.average(radial_data.y)
    radial_data.dy *= factor
    if 'SAXS' in sname:
        radial_data.dx = 4e-3*np.ones(radial_data.x.size)
        dy_magnitude = 1
        radial_data.dy = dy_magnitude*np.ones(radial_data.x.size)#/radial_data.x**2#0.001*# no
        # useful
        # error
        # bars
        # available
        #radial_data.dy = dy_magnitude*radial_data.y.copy()
        #if kratky:
        #    radial_data.dy *= radial_data.x**2#dy_magnitude*np.ones(radial_data.x.size)
    if sub is not None:
        radial_data.y -= sub
        
    if kratky:
        radial_data.y *= (radial_data.x)**2
        #radial_data.dy *= (radial_data.x)**2
        average = np.average(radial_data.y)
        radial_data.y /= average # normalize to avoid very small residuals 

    
    return radial_data, average  

def get_cylinder_background(sample_data,
                            c_cyl = 9.27,
                            cyl_store='./pfo_cylinder/pfo_12p5_cylinder',
                            kratky=True):
    """retrieve cylinder data from cyl_store for the q-range of sample_data,
    scale to c_cyl"""
    cyl_exp = load_problem(cyl_store+'.py')
    load_best(cyl_exp, cyl_store+'.par')
    cyl_adj_q = Experiment(data = sample_data, model=cyl_exp.fitness.model)
    y_cyl = cyl_adj_q.theory()
    if kratky:
        y_cyl *= sample_data.x**2
    return y_cyl*c_cyl/12.5 # scale from 12.5 to c_cyl mM
    
    


def model_dataset(params, x, kernel):
    data = empty_data1D(x)
    model = kernel.make_kernel([x])
    return call_kernel(model, params)
import sample.sample as s; sample = s.make_sample(); pfo = s.surfactant_make_lipfo()
import model.distributions as model_dist; factor = model_dist.factor
#import sample.sample as s; sample = s.make_sample()

def xL_free(sample):
    return 1/(1-sample.x1_sample) - sample.x1_sample/(sample.x1_agg * (1-sample.x1_sample))

def phi_agg(sample):
    c_surf2_monomer = sample.c_tot * (1-sample.x1_sample) * xL_free(sample)
    return c.N_A*(sample.c_tot - c_surf2_monomer)*1e-24*(sample.surf1.v_molecule * sample.x1_agg + sample.surf2.v_molecule * (1-sample.x1_agg))

def set_empirical_sample_properties(sample, radiation='n'):
    sample.x1_agg = xT
    sample.x_free = xL_free(sample)*(1-sample.x1_sample)
    sample.surf2.c_monomer = xL_free(sample)*sample.c_tot*(1-sample.x1_sample)
    sample.phi_agg = phi_agg(sample)    
    sample.sld_agg = s.sld_agg(sample, radiation=radiation)
    return sample 
 


xT_label = int(np.round(xT*100))
set_empirical_sample_properties(sample, radiation='x')
import copy 

kratky= (scale=="kratky")

phi_ves = sample.phi_agg
c_pfo = sample.surf2.c_monomer*1e3  # mmol/L

sname = "SAXS"
radial_data, average = get_radial_data(sname, kratky=kratky)
set_beam_stop(radial_data, 0.04, outer=2.5)

if kratky:    
    kernel = load_model("custom.vesicles_3sh_asymm_volume_kratky+custom.cylinder_kratky")
else:
    kernel = load_model("custom.vesicles_3sh_asymm_volume+cylinder")

previous = f"./sasmodels_vesicle_3sh_symm_bilayer-pd_kratky_SAXS_{xT_label:03d}/sim_3sh_bilayer-pd_symm_{xT_label:03d}.err"
cylinder = "./pfo_cylinder/pfo_12p5_cylinder.err"

model = Model(kernel,
              background = 0,
              scale = 1e3/average,
              A_scale = param_from_errfile(previous, 'A_scale'),
              A_volume = param_from_errfile(previous, 'A_volume'), 
              A_volume_pd = param_from_errfile(previous, 'A_volume_pd'), 
              A_volume_pd_type = 'schulz',
              A_thickness_bilayer = param_from_errfile(previous, 'A_thickness_bilayer'),
              A_thickness_bilayer_pd = param_from_errfile(previous, 'A_thickness_bilayer_pd'),
              A_sld_bilayer = param_from_errfile(previous, 'A_sld_bilayer'),
              A_sld_solvent = param_from_errfile(previous, 'A_sld_solvent'),
              A_x_surf1 = param_from_errfile(previous, 'A_x_surf1'),
              A_sld_surf1 = param_from_errfile(previous, 'A_sld_surf1'),
              A_sld_surf2 = param_from_errfile(previous, 'A_sld_surf2'),
              A_v_surf1 = param_from_errfile(previous, 'A_v_surf1'),
              A_v_surf2 = param_from_errfile(previous, 'A_v_surf2'),
              A_x_methyl_2 = param_from_errfile(previous, 'A_x_methyl_2'),
              A_sl_methyl = param_from_errfile(previous, 'A_sl_methyl'),
              A_sl_methylene = param_from_errfile(previous, 'A_sl_methylene'),
              A_x_surf2_3 = 1 - param_from_errfile(previous, 'A_x_surf1'), # start with symmetric case
              A_thickness13 = param_from_errfile(previous, 'A_thickness13'),
              B_scale = param_from_errfile(previous, 'B_scale'),
              B_sld = param_from_errfile(cylinder, 'sld'),
              B_sld_solvent = param_from_errfile(cylinder, 'sld_solvent'),
              B_radius = param_from_errfile(cylinder, 'radius'),
              B_length = param_from_errfile(cylinder, 'length'),
              )

def radius_from_v(volume, t):
    radius = np.sqrt((volume - c.pi/3.*t**3)/(4*c.pi*t))
    return radius

r = radius_from_v(model.A_volume.value, model.A_thickness_bilayer.value) - model.A_thickness_bilayer.value/2.
ro = r + model.A_thickness_bilayer.value
dist = model_dist.mixture()
distribution = dist(x_avg=1-param_from_errfile(previous, 'A_x_surf1'),r=r,ro=ro, factor=factor)
model.A_x_surf2_3.pdf(distribution)
model.A_x_surf2_3._bounds.limits = distribution.support()
model.A_thickness_bilayer.range(2,4.38)

M = Experiment(data=radial_data, model=model)

def constraints():
    t, t13 = M.parameters()['A_thickness_bilayer'].value, M.parameters()['A_thickness13'].value
    x_2_o = M.parameters()['A_x_surf2_3'].value
    return 0 if (2*t13 < t) and (abs(0.5 - x_2_o) < 0.5)  else 1e6 + (t-2*t13)**6 + (abs(0.5-x_2_o)*10)**6  


problem = FitProblem(M, constraints=constraints)
problem.store = './sasmodels_vesicle_3sh_asymm_dist_kratky_{}_{:03d}'.format(sname, xT_label)
