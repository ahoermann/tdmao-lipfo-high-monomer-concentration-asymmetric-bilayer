scale="kratky"
xT=0.639804325712179
from operator import itemgetter
import scipy.constants as c
import numpy as np
from bumps.names import FitProblem
import sys
from scipy.optimize import fsolve
from scipy.special import logit, expit
import scipy.constants as c 
import pandas as pd 
sys.path.append('../../sasview-5.0.4/src')
sys.path.append('~/sasmodels/custom_models')
sys.path.append('.')
from sasmodels.data import load_data
#from bumps.names import *
from sasmodels.core import load_model
from sasmodels.bumps_model import Model, Experiment
from sasmodels.data import load_data, set_beam_stop, set_top
import os 
import bumps 
from bumps.fitproblem import load_problem
from bumps.cli import load_best
from scipy.stats import gmean

def separate_composite_parameters(par_df):
    """Split a dataframe containing parameters from a composite
model. Input is expected cleaned from leading dots and fit ranges"""
    groups = par_df.name.apply(lambda x: x.split('_')[0]).unique()
    grouped = par_df.groupby(par_df.name.apply(lambda x: x.split('_')[0]))
    separated = {}
    for key, item in grouped:
        if len(key) < 2:
            item.at['{}_scale'.format(key),'value'] *= grouped.get_group('scale').loc['scale'].value
            item.index = item.name.apply(lambda x: x.lstrip('{}_'.format(key)))
            item = item.append(grouped.get_group('background'))
            item.drop(columns=['name'], inplace=True)
            separated[key] = item
    return separated

def errfile_to_df(errfile):
    "does not work for simultaneous fits, load model instead"
    params = pd.read_csv(errfile, sep=' = ', names=['name', 'value'], engine='python')
    params = params.dropna()
    params = params[params.name.apply(lambda x: x.startswith('.'))]  # all parameter lines start with .
    params.name = params['name'].apply(lambda x: x.lstrip('.'))
    params.index = params.name
    params.value = params['value'].apply(lambda x: float(x.split('in')[0].strip()))
    return params 

def param_from_errfile(errfile, parname):
    params = errfile_to_df(errfile)
    val = params.loc[parname].value
    return val

def params_from_simultaneous(kernelname, store, modelname):
    problem = load_problem(os.path.join(store, modelname+".py"))
    load_best(problem, os.path.join(store, modelname+".par")) # use fit result
    params = problem.model_parameters()
    shared_params = params['models'][0]
    shared_params = { key: dict(name=value.name, value=value.value) for key, value in shared_params.items()}
    param_df = pd.DataFrame(shared_params).T
    free = pd.DataFrame(params['freevars'])
    free = free.set_index(free[free.columns[0]].apply(lambda x: x.name.split()[0]))
    for column in free.columns:
        free[column] = free[column].apply(lambda x: x.value)
    free = free.T.copy()
    params_by_model = {}
    for i, model in enumerate(free.columns):
        model_df = param_df.copy()
        model_free = pd.DataFrame({'name': free[model].index.values, 'value': free[model]})
        if model == 'SAXS': # reset scale 
            model_free.at['scale', 'value'] = 1e3
        # model_df.at[model_free.index[:-1],'value'] = model_free.loc[model_free.index[:-1]]['value']
        # somehow broken - why did I need that again? 
        params_by_model[model] = model_df
    return params_by_model 
    
def plot_contributions(model_df, q, ax, color):
    "plot the entire composite model (best-fit) and its contributions"
    fit = model_dataset(model_df.to_dict()['value'], q, kernel)
    l, = ax.plot(q, fit,
                 color=color,
                 alpha=1,
                 )
    grouped_params = separate_composite_parameters(model_df)
    for label, item in grouped_params.items():
        #smearing = qsmear.smear_selection(datasets[i], kernels[label].make_kernel([data_sas['q'][model]]))
        fit = model_dataset(item.to_dict()['value'], q, kernels[label])
        ax.plot(q, fit,
                color=l.get_color(),
                alpha=1, linestyle='dashed')
    

def get_radial_data(sname, kratky=True, sub=None):
    if 'AH' in sname:
        path = f'../data_new/merged/{sname}_merged.txt'
        factor = 1
        sub_bkg = True
    elif 'SAXS' in sname:
        path = '../../LiPFO_TDMAO/LPFOTDMAOdata/avedat/20111130_0_0073_rebin_sub.dat'
        factor = 1/0.13  # divide by capillary thickness in cm
        sub_bkg = False
    radial_data = load_data(path)
    radial_data.y -= sub_bkg * np.average(radial_data.y[np.where(radial_data.x > 2.5)[0]])
    radial_data.y *= factor
    average = np.average(radial_data.y)
    radial_data.dy *= factor
    if 'SAXS' in sname:
        radial_data.dx = 4e-3*np.ones(radial_data.x.size)
        dy_magnitude = 1
        radial_data.dy = dy_magnitude*np.ones(radial_data.x.size)#/radial_data.x**2#0.001*# no
        # useful
        # error
        # bars
        # available
        #radial_data.dy = dy_magnitude*radial_data.y.copy()
        #if kratky:
        #    radial_data.dy *= radial_data.x**2#dy_magnitude*np.ones(radial_data.x.size)
    if sub is not None:
        radial_data.y -= sub
        
    if kratky:
        radial_data.y *= (radial_data.x)**2
        #radial_data.dy *= (radial_data.x)**2
        average = np.average(radial_data.y)
        radial_data.y /= average # normalize to avoid very small residuals 

    
    return radial_data, average  

def get_cylinder_background(sample_data,
                            c_cyl = 9.27,
                            cyl_store='./pfo_cylinder/pfo_12p5_cylinder',
                            kratky=True):
    """retrieve cylinder data from cyl_store for the q-range of sample_data,
    scale to c_cyl"""
    cyl_exp = load_problem(cyl_store+'.py')
    load_best(cyl_exp, cyl_store+'.par')
    cyl_adj_q = Experiment(data = sample_data, model=cyl_exp.fitness.model)
    y_cyl = cyl_adj_q.theory()
    if kratky:
        y_cyl *= sample_data.x**2
    return y_cyl*c_cyl/12.5 # scale from 12.5 to c_cyl mM
    
    


def model_dataset(params, x, kernel):
    data = empty_data1D(x)
    model = kernel.make_kernel([x])
    return call_kernel(model, params)
import sample.sample as s; sample = s.make_sample(); pfo = s.surfactant_make_lipfo()
import model.distributions as model_dist; factor = model_dist.factor
#import sample.sample as s; sample = s.make_sample()

def xL_free(sample):
    return 1/(1-sample.x1_sample) - sample.x1_sample/(sample.x1_agg * (1-sample.x1_sample))

def phi_agg(sample):
    c_surf2_monomer = sample.c_tot * (1-sample.x1_sample) * xL_free(sample)
    return c.N_A*(sample.c_tot - c_surf2_monomer)*1e-24*(sample.surf1.v_molecule * sample.x1_agg + sample.surf2.v_molecule * (1-sample.x1_agg))

def set_empirical_sample_properties(sample, radiation='n'):
    sample.x1_agg = xT
    sample.x_free = xL_free(sample)*(1-sample.x1_sample)
    sample.surf2.c_monomer = xL_free(sample)*sample.c_tot*(1-sample.x1_sample)
    sample.phi_agg = phi_agg(sample)    
    sample.sld_agg = s.sld_agg(sample, radiation=radiation)
    return sample 
 

import periodictable as pt 
xT_label = int(np.round(xT*100))
set_empirical_sample_properties(sample, radiation='x')


import copy 
phi_ves = sample.phi_agg
c_pfo = sample.surf2.c_monomer*1e3 # mM 


def v_hydrocarbon(nc):
    "hydrocarbon tail volume according to Tanford in nm^3"
    return (27.4 + 26.9*nc)/1e3

def v_fluorocarbon(nc):
    "fluorocarbon tail volume according to Srinivasan in nm^3"
    return (42.4 + 41.6*nc)/1e3

def sl_hydrocarbon(nc):
    formula = f"C{nc}H{2*nc+1}"
    volume = v_hydrocarbon(nc) # nm^3
    molecule = pt.formula(formula)
    molecule.density = molecule.molecular_mass/(volume*1e-21)
    sld = pt.xray_sld(molecule, energy=12)[0]
    return sld*volume 

def sl_fluorocarbon(nc):
    formula = f"C{nc}F{2*nc+1}"
    volume = v_fluorocarbon(nc) # nm^3
    molecule = pt.formula(formula)
    molecule.density = molecule.molecular_mass/(volume*1e-21)
    sld = pt.xray_sld(molecule, energy=12)[0]
    return sld*volume 


previous = f"./sasmodels_vesicle_3sh_asymm_dist_kratky_SAXS_{xT_label:03d}/sim_3sh_dist_asymm_{xT_label:03d}.err"
cylinder = "./pfo_cylinder/pfo_12p5_cylinder.err"

sname = "SAXS"
kratky= (scale=="kratky")
radial_data, average = get_radial_data(sname, kratky=kratky)
set_beam_stop(radial_data, 0.04, outer=2.5)

kernel = load_model("custom.vesicle_5sh_volume_kratky+custom.cylinder_kratky")

model = Model(kernel,
              background = 0,
              scale = 1e3/average,
              A_scale = param_from_errfile(previous, 'A_scale'),
              A_volume = param_from_errfile(previous, 'A_volume'),
              A_volume_pd = param_from_errfile(previous, 'A_volume_pd'),
              A_volume_pd_type = 'schulz',
              A_thickness_bilayer = param_from_errfile(previous, 'A_thickness_bilayer'),
              A_thickness_bilayer_pd = param_from_errfile(previous, 'A_thickness_bilayer_pd'),
              A_sld_bilayer = param_from_errfile(previous, 'A_sld_bilayer'),
              A_sld_solvent = param_from_errfile(previous, 'A_sld_solvent'),
              A_x_surf1 = sample.x1_agg,
              A_sld_surf1 = param_from_errfile(previous, 'A_sld_surf1'),
              A_sld_surf2 = param_from_errfile(previous, 'A_sld_surf2'),
              A_v_surf1 = sample.surf1.v_molecule,
              A_v_surf2 = sample.surf2.v_molecule,
              A_x_methyl_c = param_from_errfile(previous, 'A_x_methyl_2'),
              A_sl_surf1_tail = sl_hydrocarbon(nc=13),
              A_sl_surf2_tail = sl_fluorocarbon(nc=7),
              A_x_surf2_o = param_from_errfile(previous, 'A_x_surf2_3'),
              A_thickness_mixedtails = 0.984,
              A_sl_methyl = param_from_errfile(previous, 'A_sl_methyl'),
              A_sl_methylene = param_from_errfile(previous, 'A_sl_methylene'),
              A_v_surf1_tail = v_hydrocarbon(nc=13),
              A_v_surf2_tail = v_fluorocarbon(nc=7),
              B_scale = param_from_errfile(previous, 'B_scale'),
              B_sld = param_from_errfile(cylinder, 'sld'),
              B_sld_solvent = param_from_errfile(cylinder, 'sld_solvent'),
              B_radius = param_from_errfile(cylinder, 'radius'),
              B_length = param_from_errfile(cylinder, 'length'),
              )

#model.A_volume_pd.pmp(20)
#model.A_volume.pmp(20)
#model.A_thickness13.range(0.984, 0.984+0.3)
model.A_thickness_bilayer.range(2,4.38)

def radius_from_v(volume, t):
    radius = np.sqrt((volume - c.pi/3.*t**3)/(4*c.pi*t))
    return radius

r = radius_from_v(model.A_volume.value, model.A_thickness_bilayer.value) - model.A_thickness_bilayer.value/2.
ro = r + model.A_thickness_bilayer.value
dist = model_dist.mixture()
distribution = dist(x_avg=(1-model.A_x_surf1.value), r=r, ro=ro, factor=factor)
model.A_x_surf2_o.pdf(distribution)
model.A_x_surf2_o._bounds.limits = distribution.support()


M = Experiment(data=radial_data, model=model)

def constraints():
    volume, thickness_bilayer, t13, x_surf1, x_surf2_o  = map(lambda item: item.value,itemgetter('A_volume',
                                                                                                 'A_thickness_bilayer',
                                                                                                 'A_thickness_mixedtails',
                                                                                                 'A_x_surf1',
                                                                                                 'A_x_surf2_o',)(M.parameters())
                                                              )
    v_surf1, v_surf2, v_surf1_tail, v_surf2_tail = map(lambda item: item.value, itemgetter('A_v_surf2',
                                                              'A_v_surf2',
                                                              'A_v_surf1_tail',
                                                              'A_v_surf2_tail' )(M.parameters())
                                                       )
    def radius_from_v(volume, t):
        radius = np.sqrt((volume - c.pi/3.*t**3)/(4*c.pi*t))
        return radius

    # central and headgroup shells must have positive thicknesses
    radius = radius_from_v(volume, thickness_bilayer)

    four_pi_third = 4*np.pi/3.
    core_radius = radius - thickness_bilayer/2.
    ro = radius + thickness_bilayer/2. 
    v_bilayer = (ro**3 - core_radius**3) * four_pi_third

    def cubic_real_root(a, b, c, d):
        def q(a,b,c):
            return (3*a*c-b**2)/(9*a**2)
        def r(a,b,c,d):
            return (9*a*b*c - 27*a**2*d - 2*b**3)/(54*a**3)
        s = np.cbrt(r(a,b,c,d) + np.sqrt(q(a,b,c)**3 + r(a,b,c,d)**2))
        t = np.cbrt(r(a,b,c,d) - np.sqrt(q(a,b,c)**3 + r(a,b,c,d)**2))
        real_root = s + t - b/(3*a)
        return real_root

    v_avg = x_surf1 * v_surf1 + (1-x_surf1) * v_surf2
    n_surf2 = v_bilayer/v_avg * (1-x_surf1)
    n_surf1 = v_bilayer/v_avg * (x_surf1)
    surface_out = 4*np.pi*ro**2
    surface_in = 4*np.pi*core_radius**2
    a_avg = (surface_out + surface_in)/(n_surf1 + n_surf2)
    n_out = surface_out/a_avg
    n_in = surface_in/a_avg
    v_hg_o = n_out * ((1-x_surf2_o) * (v_surf1 - v_surf1_tail) + x_surf2_o * (v_surf2 - v_surf2_tail))
    x_surf2_i = (n_surf2 - x_surf2_o*n_out)/n_in
    v_hg_i = n_in * ((1-x_surf2_i) * (v_surf1 - v_surf1_tail) + x_surf2_i * (v_surf2 - v_surf2_tail))
    thickness_hg_i = cubic_real_root(a=1, b=3*core_radius, c=3*core_radius**2, d=-v_hg_i/four_pi_third)
    thickness_hg_o = cubic_real_root(a=-1, b=3*ro, c=-3*ro**2, d=v_hg_o/four_pi_third)
    t_center = thickness_bilayer - thickness_hg_i - thickness_hg_o - 2*t13

    # add constraint for surfactant distribution, ratio according to surface ratio
    # x_2_o * surf_o + x_2_i * surf_i = (1-x_1) * surf
    x_2_o, x_1 =  M.parameters()['A_x_surf2_o'].value, M.parameters()['A_x_surf1'].value
    ro = radius + thickness_bilayer
    x_2_i = ((1-x_1)*(radius**2 + ro**2) - x_2_o*ro**2)/(radius**2)
    return 0 if t_center > 0 and abs(0.5 - x_2_i) <= 0.5  else 1e6 + (t_center)**6 + (abs(0.5 - x_2_i) - 0.5)**6  


problem = FitProblem(M, constraints=constraints)
problem.store = f'./sasmodels_vesicle_5sh_kratky_SAXS_{xT_label:03d}'
