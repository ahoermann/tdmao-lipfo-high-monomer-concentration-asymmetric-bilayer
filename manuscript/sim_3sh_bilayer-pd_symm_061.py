xT=0.609689426557996
scale="kratky"
import scipy.constants as c
import numpy as np
from bumps.names import FitProblem
import sys
from scipy.optimize import fsolve
from scipy.special import logit, expit
import scipy.constants as c 
import pandas as pd 
sys.path.append('../../sasview-5.0.4/src')
sys.path.append('~/sasmodels/custom_models')
sys.path.append('.')
from sasmodels.data import load_data
#from bumps.names import *
from sasmodels.core import load_model
from sasmodels.bumps_model import Model, Experiment
from sasmodels.data import load_data, set_beam_stop, set_top
import os 
import bumps 
from bumps.fitproblem import load_problem
from bumps.cli import load_best
from scipy.stats import gmean

def separate_composite_parameters(par_df):
    """Split a dataframe containing parameters from a composite
model. Input is expected cleaned from leading dots and fit ranges"""
    groups = par_df.name.apply(lambda x: x.split('_')[0]).unique()
    grouped = par_df.groupby(par_df.name.apply(lambda x: x.split('_')[0]))
    separated = {}
    for key, item in grouped:
        if len(key) < 2:
            item.at['{}_scale'.format(key),'value'] *= grouped.get_group('scale').loc['scale'].value
            item.index = item.name.apply(lambda x: x.lstrip('{}_'.format(key)))
            item = item.append(grouped.get_group('background'))
            item.drop(columns=['name'], inplace=True)
            separated[key] = item
    return separated

def errfile_to_df(errfile):
    "does not work for simultaneous fits, load model instead"
    params = pd.read_csv(errfile, sep=' = ', names=['name', 'value'], engine='python')
    params = params.dropna()
    params = params[params.name.apply(lambda x: x.startswith('.'))]  # all parameter lines start with .
    params.name = params['name'].apply(lambda x: x.lstrip('.'))
    params.index = params.name
    params.value = params['value'].apply(lambda x: float(x.split('in')[0].strip()))
    return params 

def param_from_errfile(errfile, parname):
    params = errfile_to_df(errfile)
    val = params.loc[parname].value
    return val

def params_from_simultaneous(kernelname, store, modelname):
    problem = load_problem(os.path.join(store, modelname+".py"))
    load_best(problem, os.path.join(store, modelname+".par")) # use fit result
    params = problem.model_parameters()
    shared_params = params['models'][0]
    shared_params = { key: dict(name=value.name, value=value.value) for key, value in shared_params.items()}
    param_df = pd.DataFrame(shared_params).T
    free = pd.DataFrame(params['freevars'])
    free = free.set_index(free[free.columns[0]].apply(lambda x: x.name.split()[0]))
    for column in free.columns:
        free[column] = free[column].apply(lambda x: x.value)
    free = free.T.copy()
    params_by_model = {}
    for i, model in enumerate(free.columns):
        model_df = param_df.copy()
        model_free = pd.DataFrame({'name': free[model].index.values, 'value': free[model]})
        if model == 'SAXS': # reset scale 
            model_free.at['scale', 'value'] = 1e3
        # model_df.at[model_free.index[:-1],'value'] = model_free.loc[model_free.index[:-1]]['value']
        # somehow broken - why did I need that again? 
        params_by_model[model] = model_df
    return params_by_model 
    
def plot_contributions(model_df, q, ax, color):
    "plot the entire composite model (best-fit) and its contributions"
    fit = model_dataset(model_df.to_dict()['value'], q, kernel)
    l, = ax.plot(q, fit,
                 color=color,
                 alpha=1,
                 )
    grouped_params = separate_composite_parameters(model_df)
    for label, item in grouped_params.items():
        #smearing = qsmear.smear_selection(datasets[i], kernels[label].make_kernel([data_sas['q'][model]]))
        fit = model_dataset(item.to_dict()['value'], q, kernels[label])
        ax.plot(q, fit,
                color=l.get_color(),
                alpha=1, linestyle='dashed')
    

def get_radial_data(sname, kratky=True, sub=None):
    if 'AH' in sname:
        path = f'../data_new/merged/{sname}_merged.txt'
        factor = 1
        sub_bkg = True
    elif 'SAXS' in sname:
        path = '../../LiPFO_TDMAO/LPFOTDMAOdata/avedat/20111130_0_0073_rebin_sub.dat'
        factor = 1/0.13  # divide by capillary thickness in cm
        sub_bkg = False
    radial_data = load_data(path)
    radial_data.y -= sub_bkg * np.average(radial_data.y[np.where(radial_data.x > 2.5)[0]])
    radial_data.y *= factor
    average = np.average(radial_data.y)
    radial_data.dy *= factor
    if 'SAXS' in sname:
        radial_data.dx = 4e-3*np.ones(radial_data.x.size)
        dy_magnitude = 1
        radial_data.dy = dy_magnitude*np.ones(radial_data.x.size)#/radial_data.x**2#0.001*# no
        # useful
        # error
        # bars
        # available
        #radial_data.dy = dy_magnitude*radial_data.y.copy()
        #if kratky:
        #    radial_data.dy *= radial_data.x**2#dy_magnitude*np.ones(radial_data.x.size)
    if sub is not None:
        radial_data.y -= sub
        
    if kratky:
        radial_data.y *= (radial_data.x)**2
        #radial_data.dy *= (radial_data.x)**2
        average = np.average(radial_data.y)
        radial_data.y /= average # normalize to avoid very small residuals 

    
    return radial_data, average  

def get_cylinder_background(sample_data,
                            c_cyl = 9.27,
                            cyl_store='./pfo_cylinder/pfo_12p5_cylinder',
                            kratky=True):
    """retrieve cylinder data from cyl_store for the q-range of sample_data,
    scale to c_cyl"""
    cyl_exp = load_problem(cyl_store+'.py')
    load_best(cyl_exp, cyl_store+'.par')
    cyl_adj_q = Experiment(data = sample_data, model=cyl_exp.fitness.model)
    y_cyl = cyl_adj_q.theory()
    if kratky:
        y_cyl *= sample_data.x**2
    return y_cyl*c_cyl/12.5 # scale from 12.5 to c_cyl mM
    
    


def model_dataset(params, x, kernel):
    data = empty_data1D(x)
    model = kernel.make_kernel([x])
    return call_kernel(model, params)
import sample.sample as s; sample = s.make_sample(); pfo = s.surfactant_make_lipfo()
import os, copy
#import sample.sample as s; sample = s.make_sample()

def xL_free(sample):
    return 1/(1-sample.x1_sample) - sample.x1_sample/(sample.x1_agg * (1-sample.x1_sample))

def phi_agg(sample):
    c_surf2_monomer = sample.c_tot * (1-sample.x1_sample) * xL_free(sample)
    return c.N_A*(sample.c_tot - c_surf2_monomer)*1e-24*(sample.surf1.v_molecule * sample.x1_agg + sample.surf2.v_molecule * (1-sample.x1_agg))

def set_empirical_sample_properties(sample, radiation='n'):
    sample.x1_agg = xT
    sample.x_free = xL_free(sample)*(1-sample.x1_sample)
    sample.surf2.c_monomer = xL_free(sample)*sample.c_tot*(1-sample.x1_sample)
    sample.phi_agg = phi_agg(sample)    
    sample.sld_agg = s.sld_agg(sample, radiation=radiation)
    return sample 
 

import periodictable as pt 

ch3_v=52.7e-3;ch2_v = 28.1e-3; # nm^3 - armen

methyl = pt.formula("CH3")
methyl.density = methyl.molecular_mass/(ch3_v*1e-21)

methylene = pt.formula("CH2")
methylene.density = methylene.molecular_mass/(ch2_v*1e-21)

def sl_hydrocarbon_unit(unit, radiation='x'):
    if radiation == 'x':
        sld = pt.xray_sld(unit, energy=12)[0]
    elif radiation == "n":
        sld = pt.neutron_sld(unit, wavelength=5)[0]
    volume = unit.molecular_mass/(unit.density*1e-21)
    return sld*volume # in 1e-4 nm

ch3_sld = pt.xray_sld(methyl, energy=12)[0]
ch2_sld = pt.xray_sld(methylene, energy=12)[0]

ch3_sl = ch3_sld*ch3_v
ch2_sl = ch2_sld*ch2_v

xT_label = int(np.round(xT*100))
set_empirical_sample_properties(sample, radiation='x')

kratky = (scale=="kratky")

phi_ves = sample.phi_agg
c_pfo = sample.surf2.c_monomer*1e3

radial_data, average = get_radial_data("SAXS", kratky=kratky)
set_beam_stop(radial_data, 0.04, outer=2.5)

if kratky:    
    kernel = load_model("custom.vesicles_3sh_symm_volume_kratky+custom.cylinder_kratky")
else:
    kernel = load_model("custom.vesicles_3sh_symm_volume+cylinder")

if xT_label == 64:     
    previous = f"./saxs_radius_pd_kratky_SAXS/saxs_radius_pd_kratky.err"
else:
    previous = f"./saxs_radius_pd_kratky_SAXS_{xT_label:03d}/saxs_radius_pd_kratky_{xT_label:03d}.err"
cylinder = "./pfo_cylinder/pfo_12p5_cylinder.err"

x_surf1_vesicle = param_from_errfile(previous, "A_x_surf1_vesicle")
x_surf1_sample = param_from_errfile(previous, "A_x_surf1_sample")
c_molar = param_from_errfile(previous, "A_c_molar")
bilayer_sld = (x_surf1_vesicle * sample.surf1.sl_x  + (1- x_surf1_vesicle) * sample.surf2.sl_x)/(
    x_surf1_vesicle * sample.surf1.v_molecule + (1-x_surf1_vesicle) * sample.surf2.v_molecule)


x_surf2_monomer = 1/(1-x_surf1_sample) - x_surf1_sample/(x_surf1_vesicle * (1-x_surf1_sample))
c_surf2_monomer = c_molar*(1-x_surf1_sample)*x_surf2_monomer
volfraction = 6.02214076e+23*(c_molar - c_surf2_monomer)*1e-24*(sample.surf1.v_molecule * x_surf1_vesicle + sample.surf2.v_molecule * (1-x_surf1_vesicle))

radius = param_from_errfile(previous, 'A_radius')
thickness = param_from_errfile(previous, 'A_thickness')
radius_pd = param_from_errfile(previous, 'A_radius_pd')
volume = 4*c.pi/3. * ((radius + thickness/2.)**3 - (radius - thickness/2.)**3)
radius_std = radius_pd * radius
volume_std = 4*c.pi/3. * 3* ((radius + thickness/2.)**2 - (radius - thickness/2.)**2) * radius_std
volume_pd = volume_std/volume

model = Model(kernel,
              background = 0,
              scale = 1e3/average, 
              A_scale = volfraction, 
              A_volume = volume, 
              A_volume_pd = volume_pd, 
              A_volume_pd_type = 'schulz',
              A_thickness_bilayer = thickness, 
              A_thickness_bilayer_pd = 0.0,
              A_sld_bilayer = bilayer_sld, 
              A_sld_solvent = param_from_errfile(previous, 'A_sld_solvent'),
              A_sld_surf1 = sample.surf1.sld_x(),
              A_sld_surf2 = sample.surf2.sld_x(),
              A_x_surf1 = x_surf1_vesicle,
              A_v_surf1 = sample.surf1.v_molecule,
              A_v_surf2 = sample.surf2.v_molecule,
              A_x_methyl_2 = 0.1667,
              A_sl_methyl = sl_hydrocarbon_unit(methyl),
              A_sl_methylene = sl_hydrocarbon_unit(methylene),
              A_thickness13 = param_from_errfile(cylinder, 'length'),
              B_scale = param_from_errfile(previous, 'B_scale'),
              B_sld = param_from_errfile(cylinder, 'sld'),
              B_sld_solvent = param_from_errfile(cylinder, 'sld_solvent'),
              B_radius = param_from_errfile(cylinder, 'radius'),
              B_length = param_from_errfile(cylinder, 'length'),
              )

def constraints():
    t, t13 = M.parameters()['A_thickness_bilayer'].value, M.parameters()['A_thickness13'].value
    return 0 if (2*t13 < t)  else 1e6 + (t-2*t13)**6  

model.A_thickness_bilayer.range(2,4.38)
model.A_thickness13.range(0.984,0.984+0.3)


M = Experiment(data=radial_data, model=model)

problem = FitProblem(M, constraints=constraints)
problem.store = './sasmodels_vesicle_3sh_symm_bilayer-pd_kratky_{}_{:03d}'.format("SAXS", xT_label)
